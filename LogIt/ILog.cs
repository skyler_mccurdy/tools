﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogIt
{
    public interface ILog
    {

        void error(string message);
        void error(string message, Exception expception);

        void warning(string message);
        void warning(string message, Exception expception);

        void info(string message);
        void info(string message, Exception expception);

        void debug(string message);
        void debug(string message, Exception expception);
    }
}
