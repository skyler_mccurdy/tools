﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogIt
{
    public enum LogLevel
    {
        All=1000,
        Debug=400,
        Information=300,
        Warning=200,
        Error=100,
        None=0
    }
}
