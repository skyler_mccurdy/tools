﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogIt
{
    class Log: ILog
    {

        protected string topic;

        public Log(string topic)
        {
            this.topic = topic;
        }

        public void error(string message)
        {
            foreach (ITarget target in LogIt.targets)
            {
                target.error(topic,message);
            }

        }
        public void error(string message, Exception expception)
        {
            foreach (ITarget target in LogIt.targets)
            {
                target.error(topic, message, expception);
            }

        }

        public void warning(string message)
        {
            foreach (ITarget target in LogIt.targets)
            {
                target.warning(topic, message);
            }

        }
        public void warning(string message, Exception expception)
        {
            foreach (ITarget target in LogIt.targets)
            {
                target.warning(topic, message, expception);
            }
        }

        public void info(string message)
        {
            foreach (ITarget target in LogIt.targets)
            {
                target.info(topic, message);
            }

        }
        public void info(string message, Exception expception)
        {
            foreach (ITarget target in LogIt.targets)
            {
                target.info(topic, message, expception);
            }

        }

        public void debug(string message)
        {
            foreach (ITarget target in LogIt.targets)
            {
                target.debug(topic, message);
            }

        }
        public void debug(string message, Exception expception)
        {
            foreach (ITarget target in LogIt.targets)
            {
                target.debug(topic, message, expception);
            }

        }

    }
}
