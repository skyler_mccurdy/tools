﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogIt
{
    public interface ITarget
    {
        void error(string topic, string message);
        void error(string topic, string message, Exception expception);

        void warning(string topic, string message);
        void warning(string topic, string message, Exception expception);

        void info(string topic, string message);
        void info(string topic, string message, Exception expception);

        void debug(string topic, string message);
        void debug(string topic, string message, Exception expception);
    }
}
