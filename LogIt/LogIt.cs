﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace LogIt
{
    public class LogIt
    {

        [ThreadStatic]
        private static Dictionary<string, ILog> logs = new Dictionary<string,ILog>();

        public static ILog getLog(string topic)
        {
            if(!logs.ContainsKey(topic))
                logs[topic] = new Log(topic);
            return logs[topic];
        }

        private static LogIt _logIt = new LogIt();
        public static LogIt logIt
        {
            get
            {
                return _logIt;
            }
        }

        private static List<ITarget> _targets = new List<ITarget>();
        public static List<ITarget> targets
        {
            get
            {
                return _targets;
            }

            set
            {
                _targets = targets;
            }
        }
    }
}
