﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LogIt.Targets
{
    public abstract class TargetBase: ITarget
    {
        private LogLevel _level = LogLevel.All;
        public LogLevel level {
            get
            {
                filterLock.AcquireReaderLock(-1);
                try
                {
                    return _level;
                }
                finally
                {
                    filterLock.ReleaseReaderLock();
                }
            }
            private set {
                filterLock.AcquireWriterLock(-1);
                try
                {
                    _level = value;
                }
                finally
                {
                    filterLock.ReleaseWriterLock();
                }
            } 
        }

        private List<string> _topics = new List<string>();
        public List<string> topics
        {
            get
            {
                filterLock.AcquireReaderLock(-1);
                try
                {
                    return _topics;
                }
                finally
                {
                    filterLock.ReleaseReaderLock();
                }
            }

            private set
            {
                filterLock.AcquireWriterLock(-1);
                try
                {
                    _topics = value;
                }
                finally
                {
                    filterLock.ReleaseWriterLock();
                }
            }
        }

        private ReaderWriterLock filterLock = new ReaderWriterLock();

        public TargetBase()
        {

        }

        public TargetBase(LogLevel level)
        {
            this.level = level;
        }

        public TargetBase(List<string> topics)
        {
            this.topics = topics;
        }

        public TargetBase(LogLevel level, List<string> topics)
        {
            this.level = level;
            this.topics = topics;
        }

        protected abstract void logMessage(string topic, LogLevel level, string message, Exception exception);

        protected void filter(string topic, LogLevel level, string message, Exception exception)
        {
            filterLock.AcquireReaderLock(-1);
            try
            {
                if (this.level >= level && (this.topics.Count()==0 || this.topics.Contains(topic)))
                {
                    logMessage(topic, level, message, exception);
                }
            }
            finally
            {
                filterLock.ReleaseReaderLock();
            }
        }


        public void error(string topic, string message)
        {
            filter(topic, LogLevel.Error, message, null);
        }
        public void error(string topic, string message, Exception exception)
        {
            filter(topic, LogLevel.Error, message, exception);
        }


        public void warning(string topic, string message)
        {
            filter(topic, LogLevel.Warning, message, null);
        }
        public void warning(string topic, string message, Exception exception)
        {
            filter(topic, LogLevel.Warning, message, exception);
        }

        public void info(string topic, string message)
        {
            filter(topic, LogLevel.Information, message, null);
        }
        public void info(string topic, string message, Exception exception)
        {
            filter(topic, LogLevel.Information, message, exception);
        }

        public void debug(string topic, string message)
        {
            filter(topic, LogLevel.Debug, message, null);
        }
        public void debug(string topic, string message, Exception exception)
        {
            filter(topic, LogLevel.Debug, message, exception);
        }
    }
}
