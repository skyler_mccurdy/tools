﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogIt.Targets
{
    public class ConsoleTarget: TargetBase
    {
        private static object consoleLock = new object();
       

        public ConsoleTarget(): base()
        {
            
        }

        public ConsoleTarget(LogLevel level): base(level)
        {
            
        }

        public ConsoleTarget(List<string> topics)
            : base(topics)
        {

        }

        public ConsoleTarget(LogLevel level,List<string> topics)
            : base(level,topics)
        {

        }

        protected override void logMessage(string topic, LogLevel level, string message, Exception exception)
        {
            lock (consoleLock)
            {
                Console.Write(topic);
                Console.Write(" (" + level + "): ");
                Console.WriteLine(message);
                if (exception != null)
                {
                    Console.WriteLine("Exception: " + exception.GetType().Name + " \"" + exception.Message + "\"");
                    Console.WriteLine(exception.StackTrace);
                }
            }
        }

        
    }
}
