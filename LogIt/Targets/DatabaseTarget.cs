﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data.Odbc;


namespace LogIt.Targets
{
    public class DatabaseTarget: TargetBase
    {
        [ThreadStatic]
        private static SqlConnection connection = null;
       

        public DatabaseTarget(string connectionInfo): base()
        {
            connect(connectionInfo);
        }

        public DatabaseTarget(string connectionInfo,LogLevel level): base(level)
        {
            connect(connectionInfo);
        }

        public DatabaseTarget(string connectionInfo,List<string> topics)
            : base(topics)
        {
            connect(connectionInfo);
        }

        public DatabaseTarget(string connectionInfo,LogLevel level,List<string> topics)
            : base(level,topics)
        {
            connect(connectionInfo);
        }

        private void connect(string info)
        {
            connection = new SqlConnection();
            connection.ConnectionString = info;
            connection.Open();
        }

        private static string cmdStringWithException =
            "declare @t datetime2(3) \n" +
            "set @t = getutcdate(); \n" +
            "exec Log.logError  @client,@userName,@t,@topic,@level,@msg,@exp,@trace;";

        private static string cmdString =
            "declare @t datetime2(3) \n" +
            "set @t = getutcdate(); \n" +
            "exec Log.logError  @client,@userName,@t,@topic,@level,@msg,null,null;";

        protected override void logMessage(string topic, LogLevel level, string message, Exception exception)
        {
            if (connection != null && connection.State == System.Data.ConnectionState.Open)
            {

                string client = System.Environment.MachineName+"[";
                foreach (var ip in System.Net.Dns.GetHostAddresses(System.Environment.MachineName))
                {
                    client += ip.ToString()+",";
                }
                if (client[client.Length - 1] == ',')
                {
                    client = client.Substring(0, client.Length - 2);
                }
                client += "]";

                SqlCommand cmd = new SqlCommand((exception!=null)?cmdStringWithException:cmdString, connection);
                cmd.Parameters.Add("@client", System.Data.SqlDbType.VarChar).Value = 
                    client;
                cmd.Parameters.Add("@userName", System.Data.SqlDbType.VarChar).Value = 
                    System.Environment.UserDomainName + "\\" + System.Environment.UserName;
                cmd.Parameters.Add("@topic", System.Data.SqlDbType.VarChar).Value =
                    topic;
                cmd.Parameters.Add("@level", System.Data.SqlDbType.Int).Value =
                    level;
                    //0;
                cmd.Parameters.Add("@msg", System.Data.SqlDbType.VarChar).Value =
                    message;
                if (exception != null)
                {
                    cmd.Parameters.Add("@exp", System.Data.SqlDbType.VarChar).Value =
                        (exception == null) ? null :
                        exception.GetType().Name + " " + exception.Message;
                    cmd.Parameters.Add("@trace", System.Data.SqlDbType.VarChar).Value =
                        (exception == null) ? null :
                        exception.StackTrace;
                }

                int r = cmd.ExecuteNonQuery();
            }
        }

        
    }
}
