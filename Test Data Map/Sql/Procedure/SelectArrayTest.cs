﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Test_DataMap.Sql.Procedure
{
    [TestClass]
    public class SelectArrayTest
    {
        [TestMethod]
        public void DataMap_Sql_Procedure_SelectArray_Test()
        {
            Mock mock = DataMap.DataMapFactoryBase.factory<SqlFactoryMock>().create<Mock>();

            mock.id = 200;
            IEnumerable<Mock> children = mock.children;

            Assert.IsNotNull(children);
            int count = 0;
            foreach (Mock c in children)
            {
                count++;
                Assert.AreEqual(count, c.id);
                Assert.AreEqual(200,c.parent_ID);
                Assert.AreEqual("Child " + count, c.name);

            }
            Assert.AreEqual(3, count);
        }

        [TestMethod]
        public void DataMap_Sql_Procedure_SelectArray_Static_Test()
        {
            
            IEnumerable<Mock> all = Mock.getAll(DataMap.DataMapFactoryBase.factory<SqlFactoryMock>());

            Assert.IsNotNull(all);
            int count = 0;
            foreach (Mock m in all)
            {
                count++;
                Assert.AreEqual(count, m.id);
                Assert.AreEqual(100, m.parent_ID);
                Assert.AreEqual("test " + count, m.name);

            }
            Assert.AreEqual(3, count);
        }

        [DataMap.Sql.Procedure.Select("select 200 as id, 'parent' as name, null as parent_ID;")]
        private class Mock : DataMap.ObjectBase
        {
            [DataMap.Sql.Column]
            [DataMap.Sql.Procedure.SelectArray.Parameter("@selectChildren_id",System.Data.SqlDbType.Int)]
            public int id { get; set; }

            [DataMap.Sql.Column]
            public string name { get; set; }

            [DataMap.Sql.Column]
            public int? parent_ID { get; set; }

            [DataMap.Sql.Procedure.SelectArray("exec selectArray @selectChildren_id")]
            public IEnumerable<Mock> children
            {
                get
                {
                    return dataInterface.getArray<Mock>();
                }
            }

            [DataMap.Sql.Procedure.SelectArray("exec selectArray_all")]
            public static IEnumerable<Mock> getAll(DataMap.IDataMapFactory factory)
            {
                return factory.getArray<Mock>(typeof(Mock));
            }
        }
    }
}
