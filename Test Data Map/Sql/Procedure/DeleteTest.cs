﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test_DataMap.Sql.Procedure
{
    [TestClass]
    public class DeleteTest
    {

        [TestMethod]
        public void DataMap_Sql_Procedure_Delete_Test()
        {
            MockDelete mock = DataMap.DataMapFactoryBase.factory<SqlFactoryMock>().create<MockDelete>(new object[]{42});

            mock.stringValue = "The answer";


            Assert.IsTrue(mock.delete());

            Assert.AreEqual(42, mock.id);
            Assert.AreEqual("The answer", mock.stringValue);
        }


    

        [DataMap.Sql.Procedure.Delete("exec deleteTest_delete @id")]
        public class MockDelete:DataMap.ObjectBase
        {
            [DataMap.Sql.Column]
            [DataMap.Sql.Procedure.Delete.Parameter("@id",System.Data.SqlDbType.Int)]
            public int id { get; set; }

            [DataMap.Sql.Column]
            public string stringValue { get; set; }

            public MockDelete()
            {
                stringValue = null;
            }

            public MockDelete(int id)
            {
                this.id = id;
                stringValue = null;
            }
        }
    }
}
