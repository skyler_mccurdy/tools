﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test_DataMap.Sql.Procedure
{
    [TestClass]
    public class CommitTest
    {
        
        [TestMethod]
        public void DataMap_Sql_Procedure_Commit_New()
        {
            MockUpdate mock = DataMap.DataMapFactoryBase.factory<SqlFactoryMock>().create<MockUpdate>();

            mock.stringValue = "The answer";


            Assert.IsTrue(mock.commit());

            Assert.AreEqual(7, mock.id);
            Assert.AreEqual("The answer", mock.stringValue);
        }

        [TestMethod]
        public void DataMap_Sql_Procedure_Commit_Update()
        {
            MockUpdate mock = DataMap.DataMapFactoryBase.factory<SqlFactoryMock>().create<MockUpdate>(new object[] { 42 });

            mock.stringValue = "The answer";

            Assert.IsTrue(mock.commit());

            Assert.AreEqual(42, mock.id);
            Assert.AreEqual("The answer", mock.stringValue);
        }

        [DataMap.Sql.Procedure.Select("select 1;")]
        [DataMap.Sql.Procedure.Insert("exec commitTest_commit 7, @stringValue")]
        [DataMap.Sql.Procedure.Update("exec commitTest_commit @id, @stringValue")]
        public class MockUpdate:DataMap.ObjectBase
        {
            [DataMap.Sql.Column]
            [DataMap.Sql.Procedure.Update.Parameter("@id",System.Data.SqlDbType.Int)]
            public int id { get; set; }


            [DataMap.Sql.Column]
            [DataMap.Sql.Procedure.Insert.Parameter("@stringValue",System.Data.SqlDbType.VarChar)]
            [DataMap.Sql.Procedure.Update.Parameter("@stringValue", System.Data.SqlDbType.VarChar)]
            public string stringValue { get; set; }

            public MockUpdate()
            {
                stringValue = null;
                isNew = true;
                isModified = true;
            }

            public MockUpdate(int id)
            {
                this.id = id;
                stringValue = null;
                isNew = false;
                isModified = true;
            }
        }
    }
}
