﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test_DataMap.Sql.Procedure
{
    [TestClass]
    public class SelectTest
    {
        

        [TestMethod]
        public void DataMap_Sql_Procedure_Select_Test()
        {

            int id = 2;
            string varCharValue = "This is a test. This is only a test.";
            int intValue = 42;
            double doubleValue = 3.14;
            decimal decimalValue = 3;
            bool boolValue = true;
            object variant = 7;
            DateTime dateTimeValue = DateTime.Parse("2014-11-01 00:00:00.000");



            Mock mock = DataMap.DataMapFactoryBase.factory<SqlFactoryMock>().get<Mock>(new object[]{id});


            Assert.IsTrue(mock.refresh());

            Assert.AreEqual(id, mock.id);
            Assert.AreEqual(varCharValue, mock.varCharValue);
            Assert.AreEqual(intValue, mock.intValue);
            Assert.AreEqual(doubleValue, mock.doubleValue);
            Assert.AreEqual(decimalValue, mock.decimalValue);
            Assert.AreEqual(boolValue, mock.boolValue);
            Assert.AreEqual(dateTimeValue, mock.dateTimeValue);
            Assert.AreEqual(variant, mock.sqlVariantValue);

        }


        [TestMethod]
        public void DataMap_Sql_Procedure_Select_Test_NullValues()
        {

            int id = 1;
            string varCharValue = "Is Null";
            int intValue = -42;
            double doubleValue = -3.14;
            decimal decimalValue = -7;
            bool boolValue = false;
            DateTime dateTimeValue = DateTime.Parse("1942-01-01 00:00:00.000");



            Mock mock = DataMap.DataMapFactoryBase.factory<SqlFactoryMock>().create<Mock>(new object[]{id});


            Assert.IsTrue(mock.refresh());

            Assert.AreEqual(id, mock.id);
            Assert.AreEqual(varCharValue, mock.varCharValue);
            Assert.AreEqual(intValue, mock.intValue);
            Assert.AreEqual(doubleValue, mock.doubleValue);
            Assert.AreEqual(decimalValue, mock.decimalValue);
            Assert.AreEqual(boolValue, mock.boolValue);
            Assert.AreEqual(dateTimeValue, mock.dateTimeValue);


        }






        [DataMap.Sql.Procedure.Select("exec readerHelper_getByID @id")]
        private class Mock : DataMap.ObjectBase
        {
            [DataMap.Sql.Column]
            [DataMap.DefaultValue(-1)]
            [DataMap.Sql.Procedure.Select.Parameter("@id",System.Data.SqlDbType.Int)]
            public int id { get; set; }

            [DataMap.Sql.Column]
            [DataMap.DefaultValue("Is Null")]
            public string varCharValue { get; set; }

            [DataMap.Sql.Column]
            [DataMap.DefaultValue(-42)]
            public int intValue { get; set; }

            [DataMap.Sql.Column("floatValue")]
            [DataMap.DefaultValue(-3.14)]
            public double doubleValue { get; set; }

            [DataMap.Sql.Column]
            [DataMap.DefaultValue("-7.0")]
            public decimal decimalValue { get; set; }

            [DataMap.Sql.Column("bitValue")]
            [DataMap.DefaultValue(false)]
            public bool boolValue { get; set; }

            [DataMap.Sql.Column]
            [DataMap.DefaultValue("1942-01-01 00:00:00.000")]
            public DateTime dateTimeValue { get; set; }

            [DataMap.Sql.Column]
            public object sqlVariantValue { get; set; }

            public Mock(int id)
            {
                this.id = id;
            }

        }


    }
}
