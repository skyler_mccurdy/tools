﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test_DataMap.Sql.Procedure
{
    [TestClass]
    public class InsertTest
    {

        [TestMethod]
        public void DataMap_Sql_Procedure_Insert_Test()
        {
            MockInsert mock = DataMap.DataMapFactoryBase.factory<SqlFactoryMock>().create<MockInsert>();

            mock.stringValue = "The answer";


            Assert.IsTrue(mock.commit());

            Assert.AreEqual(42, mock.id);
            Assert.AreEqual("The answer", mock.stringValue);
        }


        [DataMap.Sql.Procedure.Insert("exec commitTest_insert @stringValue")]
        private class MockInsert:DataMap.ObjectBase
        {
            [DataMap.Sql.Column]
            public int id { get; set; }

            [DataMap.Sql.Column]
            [DataMap.Sql.Procedure.Insert.Parameter("@stringValue",System.Data.SqlDbType.VarChar)]
            public string stringValue { get; set; }

            public MockInsert()
            {
                stringValue = null;
                isNew = true;
                isModified = true;
            }

            public MockInsert(int id)
            {
                this.id = id;
                stringValue = null;
                isNew = false;
                isModified = true;
            }
        }
    }
}
