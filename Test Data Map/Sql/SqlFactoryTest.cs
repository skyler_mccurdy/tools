﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test_DataMap.Sql
{
    [TestClass]
    public class SqlFactoryTest
    {
        [TestMethod]
        public void DataMap_Sql_SqlFactory()
        {
            int expected_id = -1;
            string expected_name = null;

            DataMap.Sql.SqlFactory factory = DataMap.DataMapFactoryBase.factory<SqlFactoryMock>();

            MockObject obj = factory.create<MockObject>();

            Assert.IsNotNull(obj);
            Assert.AreEqual(expected_id,obj.id);
            Assert.AreEqual(expected_name, obj.name);

        }

        [DataMap.Sql.Procedure.Select("select 1;")]
        public class MockObject : DataMap.ObjectBase
        {
            public int id { get; set; }
            public string name { get; set; }

            public MockObject()
            {
                id = -1;
                name = null;
            }
        }

        
    }
}
