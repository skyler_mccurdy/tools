﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_DataMap.Sql
{
    public class SqlFactoryMock : DataMap.Sql.SqlFactory
    {

        private string connectionString = "Data Source=.;Initial Catalog=Test;Integrated Security=True";

        public SqlFactoryMock()
        {
            connection = new System.Data.SqlClient.SqlConnection(connectionString);
            connection.Open();
        }
    }
}
