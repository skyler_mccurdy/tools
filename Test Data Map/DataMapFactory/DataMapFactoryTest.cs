﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test_DBMap.DataMapFactory
{
    [TestClass]
    public class DataMapFactoryTest
    {
        [TestMethod]
        public void DataMap_DataMapFactory_Create_Basic()
        {
            int expected_id = -1;
            string expected_name = null;


            MockFactory factory = DataMap.DataMapFactoryBase.factory<MockFactory>();
            MockFactory.TestClass tc = factory.create<MockFactory.TestClass>();

            Assert.IsNotNull(tc);
            Assert.AreEqual(expected_id, tc.id);
            Assert.AreEqual(expected_name, tc.name);
        }

        [TestMethod]
        public void DataMap_DataMapFactory_Create_WithParameters()
        {
            int expected_id = -1;
            string expected_name = null;


            MockFactory factory = DataMap.DataMapFactoryBase.factory<MockFactory>();
            MockFactory.TestClass tc = factory.create<MockFactory.TestClass>(new object[] {expected_id,expected_name});

            Assert.IsNotNull(tc);
            Assert.AreEqual(expected_id, tc.id);
            Assert.AreEqual(expected_name, tc.name);
        }


        private class MockFactory : DataMap.DataMapFactoryBase
        {

            private class DataInterface : DataMap.IDataInterface
            {

                public bool refresh()
                {
                    return true;
                }
                public bool commit()
                {
                    return true;
                }
                public bool delete()
                {
                    return true;
                }

                public IEnumerable<T> getArray<T>([CallerMemberName] string propertyName = null) where T : DataMap.IObject
                {

                    return null;
                }

                public bool getElementValue<T>(out T value, [CallerMemberName] string name = null)
                {
                    value = default(T);
                    return true;
                }
                public bool setElementValue<T>(T value, [CallerMemberName] string name = null)
                {
                    return true;
                }

                public bool validate(out List<string> reasons, [CallerMemberName] string name = null)
                {
                    reasons = new List<string>();
                    return true;
                }

                public DataMap.IObject obj { get; set; }
                public DataMap.IDataMapFactory factory { get; set; }

                public DataInterface(DataMap.IObject obj)
                {
                    this.obj = obj;
                }
            }

            protected override DataMap.IDataInterface getInterfaceFor(DataMap.IObject obj)
            {
                return new DataInterface(obj);
            }


            public class TestClass:DataMap.ObjectBase
            {
                public int id { get; set; }
                public string name { get; set; }


                public TestClass()
                {
  
                    this.id = -1;
                    this.name = null;
                }

                public TestClass(int id,string name)
                {

                    this.id = id;
                    this.name = name;
                }
            }

            public override IEnumerable<T> getArray<T>(Type type, [CallerMemberName] string propertyName = null)
            {
                throw new NotImplementedException();
            }
        }
    }
}
