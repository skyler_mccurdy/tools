package com.oddzod.dataMap.tests.baseType;

import static org.junit.Assert.*;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;

import org.junit.Test;

import com.oddzod.dataMap.DataMap;
import com.oddzod.dataMap.IDataFactory;
import com.oddzod.dataMap.IDataInterface;
import com.oddzod.dataMap.IObject;
import com.oddzod.dataMap.baseType.DataFactoryBase;
import com.oddzod.dataMap.baseType.DataInterfaceBase;

public class TestDataFactoryBase {

	@Test
	public void testCreate() throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException, InvocationTargetException, IOException {
		
		Class<?> factoryClass = FactoryMock.class;
		Class<?> objectClass = ObjectMock.class;
		
		IDataFactory factory;
		IObject obj;
		
		factory = DataMap.getFactory(factoryClass);
		obj = factory.create(objectClass);
		
		assertNotNull("create result:", obj);
		assertEquals(objectClass.getName(),obj.getClass().getName());
		assertEquals(factory,obj.getFactory());
		assertNotNull("getDataInterface result",obj.getDataInterface());
		
		
	}
	
	@Test
	public void testCreate_WithArugments() throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException, InvocationTargetException, IOException {
		
		Class<?> factoryClass = FactoryMock.class;
		Class<?> objectClass = ObjectMock.class;
		Date date = new Date();
		
		IDataFactory factory;
		IObject obj;
		
		factory = DataMap.getFactory(factoryClass);
		obj = factory.create(objectClass,date.getTime());
		
		assertNotNull("create result:", obj);
		assertEquals(objectClass.getName(),obj.getClass().getName());
		assertEquals(factory,obj.getFactory());
		assertNotNull("getDataInterface result",obj.getDataInterface());
		assertEquals(date,((ObjectMock)obj).value);
		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testCreate_WithBadObject() throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException, InvocationTargetException, IOException {
		
		Class<?> factoryClass = FactoryMock.class;
		Class<?> objectClass = Object.class;
		
		IDataFactory factory;
		@SuppressWarnings("unused")
		IObject obj;
		
		factory = DataMap.getFactory(factoryClass);
		obj = factory.create(objectClass);
		
		fail("Expected create to throw IllegalArgumentException");
		
		
	}

	@Test
	public void testGet() throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException, InvocationTargetException, IOException {
		Class<?> factoryClass = FactoryMock.class;
		Class<?> objectClass = ObjectMock.class;
		
		Date value = new Date();
		
		IDataFactory factory;
		ObjectMock obj;
		
		factory = DataMap.getFactory(factoryClass);
		obj = (ObjectMock) factory.get(objectClass,value.getTime());
		
		assertNotNull("create result:", obj);
		assertEquals(objectClass.getName(),obj.getClass().getName());
		assertEquals(factory,obj.getFactory());
		assertNotNull("getDataInterface result",obj.getDataInterface());
		assertEquals("value",value,obj.value);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testGet_WithBadObject() throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException, InvocationTargetException, IOException {
		Class<?> factoryClass = FactoryMock.class;
		Class<?> objectClass = Object.class;
		
		IDataFactory factory;
		@SuppressWarnings("unused")
		IObject obj;
		
		factory = DataMap.getFactory(factoryClass);
		obj = factory.get(objectClass,0);
		
		fail("Expected get to throw IllegalArgumentException");
	}
	
	public static class FactoryMock extends DataFactoryBase{

		
		
		@Override
		public IDataInterface getDataInterface(IObject obj) {	
			return new DataInterface();
		}
		
		
		private class DataInterface extends DataInterfaceBase{

			@Override
			public boolean refresh() throws IOException {
				return true;
			}

			@Override
			public boolean commit() throws IOException {
				return false;
			}

			@Override
			public boolean delete() throws IOException {
				return false;
			}

			

			@Override
			public IDataFactory getFactory() {
				return null;
			}

			@Override
			public void setFactory(IDataFactory factory)
					throws IllegalAccessException {
			}

			@Override
			public IObject getObject() {
				return null;
			}

			@Override
			public void setObject(IObject obj) throws IllegalAccessException {
				
			}

			@Override
			public void changed(String name) {
				
			}
			
		}
		
	}
	
	public static class ObjectMock implements IObject{
		
		private IDataFactory factory;
		private IDataInterface dataInterface;
		public Date value;
		
		public ObjectMock(){
			value = null;
		}
		
		public ObjectMock(Long value){
			this.value= new Date(value);
		}

		@Override
		public boolean refresh() throws IOException {
			return true;
		}

		@Override
		public boolean commit() throws IOException {
			return false;
		}

		@Override
		public boolean delete() throws IOException {
			return false;
		}

		@Override
		public boolean isNew() {
			return false;
		}

		@Override
		public boolean isModified() {
			return false;
		}


		@Override
		public IDataFactory getFactory() {
			return factory;
		}

		@Override
		public void setFactory(IDataFactory factory)
				throws IllegalAccessException {
			this.factory = factory;
		}

		@Override
		public IDataInterface getDataInterface() {
			return dataInterface;
		}

		@Override
		public void setDataInterface(IDataInterface dataInterface)
				throws IllegalAccessException {
			this.dataInterface = dataInterface;
			
		}

		@Override
		public long getUniqueIdentifier() {
			return 0;
		}

		@Override
		public void changed(String name) {
			
		}
		
		
		
	}

}
