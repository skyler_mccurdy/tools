package com.oddzod.dataMap.tests.baseType;

import static org.junit.Assert.*;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;

import org.junit.Test;

import com.oddzod.dataMap.*;
import com.oddzod.dataMap.baseType.DataInterfaceBase;
import com.oddzod.dataMap.baseType.ObjectBase;

public class TestObjectBase {

	@Test
	public void testRefresh() throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException, InvocationTargetException, IOException {
		Class<?> factoryClass = MockFactory.class;
		Class<?> objectClass = MockObject.class;
		
		IDataFactory factory = DataMap.getFactory(factoryClass);
		IObject obj = factory.create(objectClass);
		
		obj.refresh();
		
		assertTrue("must call refresh",((MockFactory.DataInterface)obj.getDataInterface()).refreshCalled);
		assertFalse("must set isNew to false",obj.isNew());
		assertFalse("must set isModified to false",obj.isModified());
	}

	@Test
	public void testCommit() throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException, InvocationTargetException, IOException {
		Class<?> factoryClass = MockFactory.class;
		Class<?> objectClass = MockObject.class;
		
		IDataFactory factory = DataMap.getFactory(factoryClass);
		IObject obj = factory.create(objectClass);
		
		obj.commit();
		
		assertTrue("must call commit",((MockFactory.DataInterface)obj.getDataInterface()).commitCalled);
		assertFalse("must set isNew to false",obj.isNew());
		assertFalse("must set isModified to false",obj.isModified());
	}

	@Test
	public void testDelete() throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException, InvocationTargetException, IOException {
		Class<?> factoryClass = MockFactory.class;
		Class<?> objectClass = MockObject.class;
		
		IDataFactory factory = DataMap.getFactory(factoryClass);
		IObject obj = factory.create(objectClass);
		
		obj.delete();
		
		assertTrue("must call delete",((MockFactory.DataInterface)obj.getDataInterface()).deleteCalled);
	}

	@Test
	public void testChanged() throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException, InvocationTargetException, IOException {
		Class<?> factoryClass = MockFactory.class;
		Class<?> objectClass = MockObject.class;
		
		IDataFactory factory = DataMap.getFactory(factoryClass);
		IObject obj = factory.create(objectClass);
		
		obj.changed("Valid");
		
		assertTrue("must call changed",((MockFactory.DataInterface)obj.getDataInterface()).changedCalled);
	}
	

	@Test
	public void testGetFactory() throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException, InvocationTargetException, IOException {
		Class<?> factoryClass = MockFactory.class;
		Class<?> objectClass = MockObject.class;
		
		IDataFactory factory = DataMap.getFactory(factoryClass);
		IObject obj = factory.create(objectClass);
		IDataFactory factory2 = obj.getFactory();
		
		
		assertEquals("getFactory result",factory,factory2);
	}
	
	
	@Test (expected = IllegalAccessException.class)
	public void testSetFactory_CalledTwice() throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException, InvocationTargetException, IOException {
		Class<?> factoryClass = MockFactory.class;
		Class<?> objectClass = MockObject.class;
		
		IDataFactory factory = DataMap.getFactory(factoryClass);
		IObject obj = factory.create(objectClass);
		
		obj.setFactory(factory);
		
		
		fail("Expected IllegalAccessException");
	}

	@Test
	public void testGetDataInterface() throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException, InvocationTargetException, IOException {
		Class<?> factoryClass = MockFactory.class;
		Class<?> objectClass = MockObject.class;
		Class<?> dataInterfaceClass = MockFactory.DataInterface.class;
		
		IDataFactory factory = DataMap.getFactory(factoryClass);
		IObject obj = factory.create(objectClass);
		IDataInterface dataInterface = obj.getDataInterface();
		
		
		assertNotNull("getDataInterface result",dataInterface);
		assertEquals("must be the correct type",dataInterfaceClass.getName(),dataInterface.getClass().getName());
	}

	@Test (expected = IllegalAccessException.class)
	public void testSetDataInterface_CalledTwice() throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException, InvocationTargetException, IOException {
		Class<?> factoryClass = MockFactory.class;
		Class<?> objectClass = MockObject.class;
		
		IDataFactory factory = DataMap.getFactory(factoryClass);
		IObject obj = factory.create(objectClass);
		
		obj.setDataInterface(obj.getDataInterface());
		
		
		fail("Expected IllegalAccessException");
	}

	@Test
	public void testGetUniqueIdentifier() throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException, InvocationTargetException, IOException {
		Class<?> factoryClass = MockFactory.class;
		Class<?> objectClass = MockObject.class;
		long id = (new Date()).getTime();
		
		IDataFactory factory = DataMap.getFactory(factoryClass);
		IObject obj = factory.get(objectClass,id);
		long id2 = obj.getUniqueIdentifier();
		
		
		assertEquals("getUniqueIdentifier result",id,id2);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static class MockObject extends ObjectBase{

		private long uniqueIdentifier;
		
		public MockObject(){
			super();
		}
		
		public MockObject(Long id) throws IOException{
			super(id);
		}
		
		@Override
		public long getUniqueIdentifier() {
			return uniqueIdentifier;
		}

		@Override
		protected void setUniqueIdentifier(long uniqueIdentifier) {
			this.uniqueIdentifier=uniqueIdentifier;
		}
		
	}
	
	
	public static class MockFactory implements IDataFactory{

		@Override
		public IObject create(Class<?> type) throws InstantiationException,
				IllegalAccessException, IllegalArgumentException,
				NoSuchMethodException, SecurityException,
				InvocationTargetException, IOException {
			
			IObject obj = (IObject) type.newInstance();
			
			obj.setFactory(this);
			obj.setDataInterface(getDataInterface(obj));
			
			return obj;
		}

		@Override
		public IObject create(Class<?> type, Object... arguments)
				throws InstantiationException, IllegalAccessException,
				IllegalArgumentException, NoSuchMethodException,
				SecurityException, InvocationTargetException, IOException {
			return null;
		}

		@Override
		public IObject get(Class<?> type, long uniqueIdentifier)
				throws InstantiationException, IllegalAccessException,
				IllegalArgumentException, NoSuchMethodException,
				SecurityException, InvocationTargetException, IOException {
			
			
			IObject obj = (IObject) type.getConstructor(Long.class).newInstance(uniqueIdentifier);
			
			obj.setFactory(this);
			obj.setDataInterface(getDataInterface(obj));
			
			return obj;
			
		}

		@Override
		public IDataInterface getDataInterface(IObject obj) {
			DataInterface dataInterface =  new DataInterface();
			try {
				dataInterface.setFactory(this);
			} catch (IllegalAccessException e) {
				return null;
			}
			try {
				dataInterface.setObject(obj);
			} catch (IllegalAccessException e) {
				return null;
			}
			return dataInterface;
		}
		
		public class DataInterface extends DataInterfaceBase{
			
			public boolean refreshCalled=false;
			public boolean commitCalled=false;
			public boolean deleteCalled=false;
			public boolean changedCalled=false;
			

			@Override
			public boolean refresh() throws IOException {
				refreshCalled = true;
				return true;
			}

			@Override
			public boolean commit() throws IOException {
				commitCalled=true;
				return true;
			}

			@Override
			public boolean delete() throws IOException {
				deleteCalled=true;
				return true;
			}

			@Override
			public void changed(String name) {
				changedCalled=true;
				
			}

			@Override
			public IDataFactory getFactory() {
				return null;
			}

			@Override
			public void setFactory(IDataFactory factory)
					throws IllegalAccessException {
			}

			@Override
			public IObject getObject() {
				return null;
			}

			@Override
			public void setObject(IObject obj) throws IllegalAccessException {

			}
			
		}
		
	}

}
