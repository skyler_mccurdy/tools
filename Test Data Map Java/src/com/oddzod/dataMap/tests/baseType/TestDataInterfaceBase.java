package com.oddzod.dataMap.tests.baseType;

import static org.junit.Assert.*;

import java.io.IOException;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.oddzod.dataMap.DataMap;
import com.oddzod.dataMap.Getter;
import com.oddzod.dataMap.IDataFactory;
import com.oddzod.dataMap.IDataInterface;
import com.oddzod.dataMap.IObject;
import com.oddzod.dataMap.Setter;
import com.oddzod.dataMap.baseType.DataInterfaceBase;
import com.oddzod.dataMap.validation.Validation;
import com.oddzod.dataMap.validation.validationEvaluator.ValidationEvaluatorBase;

public class TestDataInterfaceBase {

	
	
	@Test
	public void testValidate_AllwaysPasses() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		String elementName = "AllwaysPasses";
		Object value = null;
		
		List<String> result = validate(elementName,value);
		assertNull("result",result);
		
	}
	
	@Test
	public void testValidate_AllwaysFails() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		String elementName = "AllwaysFails";
		Object value = null;
		List<String> expected = new ArrayList<String>();
		expected.add("allways fails");
		
		List<String> result = validate(elementName,value);
		assertNotNull("result",result);
		assertArrayEquals("result",expected.toArray(),result.toArray());
		
	}
	
	@Test
	public void testValidateAll_AllwaysPasses() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		MockObjectAllwaysPasses obj = new MockObjectAllwaysPasses();
		MockDataInterface dataInterface = new MockDataInterface();
		obj.dataInterface = dataInterface;
		dataInterface.setObject(obj);
		
		Map<String,List<String>> results = dataInterface.validateAll();
		
		assertNull("results",results);
	}
	
	@Test
	public void testValidateAll_SomeFails() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		Map<String,List<String>> expected = new HashMap<String,List<String>>();
		expected.put("AllwaysFails",new ArrayList<String>());
		expected.get("AllwaysFails").add("allways fails");
		
		MockObject obj = new MockObject();
		MockDataInterface dataInterface = new MockDataInterface();
		obj.dataInterface = dataInterface;
		dataInterface.setObject(obj);
		
		Map<String,List<String>> results = dataInterface.validateAll();
		
		assertNotNull("results",results);
		assertTrue("AllwaysFails",results.containsKey("AllwaysFails"));
		assertArrayEquals("AllwaysFails",expected.get("AllwaysFails").toArray(),results.get("AllwaysFails").toArray());
	}
	
	@Test
	public void testGetGetterForElement() throws NoSuchMethodException, SecurityException, IllegalAccessException{
		String elementName = "AllwaysPasses";
		String methodName = "allwaysPasses";
		
		MockObject obj = new MockObject();
		MockDataInterface dataInterface = new MockDataInterface();
		obj.dataInterface = dataInterface;
		dataInterface.setObject(obj);
		obj.setDataInterface(dataInterface);
		Method expected = obj.getClass().getMethod(methodName, new Class<?>[0]);
		
		Method result = dataInterface.getGetterForElement(elementName);
		
		
		assertEquals("result",expected,result);
		
	}
	
	@Test(expected = NoSuchMethodException.class)
	public void testGetGetterForElement_BadName() throws NoSuchMethodException, SecurityException, IllegalAccessException{
		String elementName = "Not Found";
		
		MockObject obj = new MockObject();
		MockDataInterface dataInterface = new MockDataInterface();
		obj.dataInterface = dataInterface;
		dataInterface.setObject(obj);
		
		dataInterface.getGetterForElement(elementName);
		
		
		fail("Expected NoSuchMethodException");
		
	}
	
	@Test
	public void testGetSetterForElement() throws NoSuchMethodException, SecurityException, IllegalAccessException{
		String elementName = "AllwaysPasses";
		String methodName = "setAllwaysPasses";
		
		MockObject obj = new MockObject();
		MockDataInterface dataInterface = new MockDataInterface();
		obj.dataInterface = dataInterface;
		dataInterface.setObject(obj);
		Method expected = obj.getClass().getMethod(methodName, String.class);
		
		Method result = dataInterface.getSetterForElement(elementName);
		
		
		assertEquals("result",expected,result);
		
	}
	
	@Test(expected = NoSuchMethodException.class)
	public void testSetGetterForElement_BadName() throws NoSuchMethodException, SecurityException, IllegalAccessException{
		String elementName = "Not Found";
		
		MockObject obj = new MockObject();
		MockDataInterface dataInterface = new MockDataInterface();
		obj.dataInterface = dataInterface;
		dataInterface.setObject(obj);
		obj.setDataInterface(dataInterface);
		
		dataInterface.getSetterForElement(elementName);
		
		
		fail("Expected NoSuchMethodException");
		
	}
	
	
	@Test
	public void testGetFactory() throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException, InvocationTargetException, IOException {
		Class<?> factoryClass = MockFactory.class;
		Class<?> objectClass = MockObject.class;
		
		IDataFactory factory = DataMap.getFactory(factoryClass);
		IObject obj = factory.create(objectClass);
		IDataFactory factory2 = obj.getDataInterface().getFactory();
		
		
		assertEquals("getFactory result",factory,factory2);
	}
	
	
	@Test (expected = IllegalAccessException.class)
	public void testSetFactory_CalledTwice() throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException, InvocationTargetException, IOException {
		Class<?> factoryClass = MockFactory.class;
		Class<?> objectClass = MockObject.class;
		
		IDataFactory factory = DataMap.getFactory(factoryClass);
		IObject obj = factory.create(objectClass);
		
		obj.getDataInterface().setFactory(factory);
		
		
		fail("Expected IllegalAccessException");
	}
	
	@Test
	public void testGetObject() throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException, InvocationTargetException, IOException {
		Class<?> factoryClass = MockFactory.class;
		Class<?> objectClass = MockObject.class;
		
		IDataFactory factory = DataMap.getFactory(factoryClass);
		IObject obj = factory.create(objectClass);
		IObject obj2 = obj.getDataInterface().getObject();
		
		
		assertEquals("getFactory result",obj,obj2);
	}
	
	
	@Test (expected = IllegalAccessException.class)
	public void testSetObject_CalledTwice() throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException, InvocationTargetException, IOException {
		Class<?> factoryClass = MockFactory.class;
		Class<?> objectClass = MockObject.class;
		
		IDataFactory factory = DataMap.getFactory(factoryClass);
		IObject obj = factory.create(objectClass);
		
		obj.getDataInterface().setObject(obj);
		
		
		fail("Expected IllegalAccessException");
	}
	
	
	
	
	
	
	
	
	
	
	private List<String> validate(String name,Object value) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
		MockObject obj = new MockObject();
		MockDataInterface dataInterface = new MockDataInterface();
		obj.dataInterface = dataInterface;
		dataInterface.setObject(obj);
		
		return dataInterface.validate(name, value);
	}
	
	public class MockObjectAllwaysPasses implements IObject{

		@Getter("element1")
		@AllwaysPasses
		public String getElement1(){
			return "element 1";
		}
		
		@Getter("element2")
		@AllwaysPasses
		public String getElement2(){
			return "element 2";
		}
		
		@Getter("element3")
		@AllwaysPasses
		public String getElement3(){
			return "element 3";
		}
		
		@Getter("element4")
		@AllwaysPasses
		public String getElement4(){
			return "element 4";
		}
		
		
		public IDataInterface dataInterface;
		
		@Override
		public boolean refresh() throws IOException {
			return false;
		}

		@Override
		public boolean commit() throws IOException {
			return false;
		}

		@Override
		public boolean delete() throws IOException {
			return false;
		}

		@Override
		public boolean isNew() {
			return false;
		}

		@Override
		public boolean isModified() {
			return false;
		}

		@Override
		public void changed(String name) {
			
		}

		@Override
		public IDataFactory getFactory() {
			return null;
		}

		@Override
		public void setFactory(IDataFactory factory)
				throws IllegalAccessException {
			
		}

		@Override
		public IDataInterface getDataInterface() {
			return dataInterface;
		}

		@Override
		public void setDataInterface(IDataInterface dataInterface)
				throws IllegalAccessException {
			
		}

		@Override
		public long getUniqueIdentifier() {
			return 0;
		}
		
	}
	
	public static class MockObject implements IObject{

		@Getter("AllwaysPasses")
		@AllwaysPasses
		public String allwaysPasses(){
			return "allways fails";
		}
		
		@Setter("AllwaysPasses")
		@AllwaysPasses
		public void setAllwaysPasses(String value){
			
		}
		
		@Getter("AllwaysFails")
		@AllwaysFails
		public String allwaysFails(){
			return "allways fails";
		}
		
		public IDataInterface dataInterface;
		
		@Override
		public boolean refresh() throws IOException {
			return false;
		}

		@Override
		public boolean commit() throws IOException {
			return false;
		}

		@Override
		public boolean delete() throws IOException {
			return false;
		}

		@Override
		public boolean isNew() {
			return false;
		}

		@Override
		public boolean isModified() {
			return false;
		}

		@Override
		public void changed(String name) {
			
		}

		
		@Override
		public IDataFactory getFactory() {
			return null;
		}

		@Override
		public void setFactory(IDataFactory factory)
				throws IllegalAccessException {
			
		}

		@Override
		public IDataInterface getDataInterface() {
			return dataInterface;
		}

		@Override
		public void setDataInterface(IDataInterface dataInterface)
				throws IllegalAccessException {
			this.dataInterface = dataInterface;
		}

		@Override
		public long getUniqueIdentifier() {
			return 0;
		}
		
	}
	
	public static class MockDataInterface extends DataInterfaceBase{

		
		
		
		@Override
		public boolean refresh() throws IOException {
			return false;
		}

		@Override
		public boolean commit() throws IOException {
			return false;
		}

		@Override
		public boolean delete() throws IOException {
			return false;
		}

		@Override
		public void changed(String name) {
			
		}

		
	}

	
	@Validation.ValidationAttribute(AllwaysPasses.Evaluator.class)
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public @interface AllwaysPasses{
		
		
		public class Evaluator extends ValidationEvaluatorBase {

			@Override
			public <V, A> String validate(V value, A attribute) {
				return null;
			}
		
		}
	}
	
	@Validation.ValidationAttribute(AllwaysFails.Evaluator.class)
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public @interface AllwaysFails{
		
		
		public class Evaluator extends ValidationEvaluatorBase {

			@Override
			public <V, A> String validate(V value, A attribute) {
				return "allways fails";
			}
		
		}
	}
	
	public static class MockFactory implements IDataFactory{

		@Override
		public IObject create(Class<?> type) throws InstantiationException,
				IllegalAccessException, IllegalArgumentException,
				NoSuchMethodException, SecurityException,
				InvocationTargetException, IOException {
			
			IObject obj = (IObject) type.newInstance();
			
			obj.setFactory(this);
			obj.setDataInterface(getDataInterface(obj));
			
			return obj;
		}

		@Override
		public IObject create(Class<?> type, Object... arguments)
				throws InstantiationException, IllegalAccessException,
				IllegalArgumentException, NoSuchMethodException,
				SecurityException, InvocationTargetException, IOException {
			return null;
		}

		@Override
		public IObject get(Class<?> type, long uniqueIdentifier)
				throws InstantiationException, IllegalAccessException,
				IllegalArgumentException, NoSuchMethodException,
				SecurityException, InvocationTargetException, IOException {
			
			
			IObject obj = (IObject) type.getConstructor(Long.class).newInstance(uniqueIdentifier);
			
			obj.setFactory(this);
			obj.setDataInterface(getDataInterface(obj));
			
			return obj;
			
		}

		@Override
		public IDataInterface getDataInterface(IObject obj) {
			DataInterface dataInterface =  new DataInterface();
			try {
				dataInterface.setFactory(this);
			} catch (IllegalAccessException e) {
				return null;
			}
			try {
				dataInterface.setObject(obj);
			} catch (IllegalAccessException e) {
				return null;
			}
			return dataInterface;
		}
		
		public class DataInterface extends DataInterfaceBase{
			
			public boolean refreshCalled=false;
			public boolean commitCalled=false;
			public boolean deleteCalled=false;
			public boolean changedCalled=false;
			

			@Override
			public boolean refresh() throws IOException {
				refreshCalled = true;
				return true;
			}

			@Override
			public boolean commit() throws IOException {
				commitCalled=true;
				return true;
			}

			@Override
			public boolean delete() throws IOException {
				deleteCalled=true;
				return true;
			}

			@Override
			public void changed(String name) {
				changedCalled=true;
				
			}

			
		}
		
	}
}
