package com.oddzod.dataMap.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import com.oddzod.dataMap.DataMap;
import com.oddzod.dataMap.IDataFactory;
import com.oddzod.dataMap.IDataInterface;
import com.oddzod.dataMap.IObject;

public class TestDataMap {

	@Test
	public void testGetFactory() throws InstantiationException, IllegalAccessException {
		
		IDataFactory factory;
		Class<?> factoryClass = MockFactory.class;
		
		factory = DataMap.getFactory(factoryClass);
		
		assertNotNull("getFactory result:",factory);
		assertTrue(factoryClass.isInstance(factory));
		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testGetFactoryWithBadClass() throws InstantiationException, IllegalAccessException, IllegalArgumentException {
		
		@SuppressWarnings("unused")
		IDataFactory factory;
		Class<?> factoryClass = Object.class;
		
		factory = DataMap.getFactory(factoryClass);
		
		fail("getFactory should have thrown a IllegalArgumentException");
		
	}
	
	
	public static class MockFactory implements IDataFactory{

		@Override
		public IObject create(Class<?> type, Object... arguments) {
			return null;
		}

		
		@Override
		public IObject create(Class<?> type) throws InstantiationException,
				IllegalAccessException, IllegalArgumentException {
			return null;
		}

		@Override
		public IObject get(Class<?> type, long uniqueIdentifier) throws InstantiationException,
				IllegalAccessException, IllegalArgumentException {
			return null;
		}

		@Override
		public IDataInterface getDataInterface(IObject obj) {

			return null;
		}
		
	}

}
