package com.oddzod.dataMap.tests.validation;

import static org.junit.Assert.*;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.oddzod.dataMap.validation.*;

public class TestStringLength {

	
	@com.oddzod.dataMap.validation.string.Length(min = -1 , max = 100)
	public String getGoodString(){
		return "A Test String";
	}
	
	@Test
	public void testValidate_WithGoodString() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		String methodName = "getGoodString";
		List<String> result;
		
		result = validate(methodName);
		
		assertNull("result",result);
		
	}
	
	
	@com.oddzod.dataMap.validation.string.Length(min = -1 , max = 5)
	public String getStringToLong(){
		return "string to long";
	}
	
	@Test
	public void testValidate_WithStringToLong() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		String methodName = "getStringToLong";
		List<String> expected = new ArrayList<String>();
		expected.add("must be at most 5 characters long");
		
		List<String> result;
		result = validate(methodName);
		
		assertNotNull("result",result);
		assertArrayEquals("result",expected.toArray(),result.toArray());
		
	}
	
	@com.oddzod.dataMap.validation.string.Length(min = 100 , max = -1)
	public String getStringToShort(){
		return "string to short";
	}
	
	@Test
	public void testValidate_WithStringToShort() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		String methodName = "getStringToShort";
		List<String> expected = new ArrayList<String>();
		expected.add("must be at least 100 characters long");
		
		List<String> result;
		result = validate(methodName);
		
		assertNotNull("result",result);
		assertArrayEquals("result",expected.toArray(),result.toArray());
		
	}
	
	@com.oddzod.dataMap.validation.string.Length(min = 5 , max = 15)
	public String getStringJustRight(){
		return "goldylocks";
	}
	
	@Test
	public void testValidate_WithStringToJustRight() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		String methodName = "getStringJustRight";
		
		List<String> result;
		result = validate(methodName);
		
		assertNull("result",result);
		
	}
	
	
	
	
	
	private List<String> validate(String methodName) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		Method method = getClass().getMethod(methodName,new Class<?>[0]);
		Object value = method.invoke(this);
		return Validation.validate(method, value);
	}

}
