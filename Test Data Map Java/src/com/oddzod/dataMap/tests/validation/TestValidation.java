package com.oddzod.dataMap.tests.validation;

import static org.junit.Assert.*;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.oddzod.dataMap.validation.Validation;
import com.oddzod.dataMap.validation.validationEvaluator.ValidationEvaluatorBase;

public class TestValidation {
	
	@AllwaysPasses
	@AllwaysFails
	public String twoValidations(){
		return "Two Validations";
	}
	
	public String noValidations(){
		return "no Validations";
	}
	
	@AllwaysPasses
	public String allwaysPasses(){
		return "allways Passes";
	}
	
	@AllwaysFails
	public String allwaysFails(){
		return "allways Failes";
	}
	
	

	@Test
	public void testGetValidations() throws NoSuchMethodException, SecurityException {
		
		List<Annotation> validations = Validation.getValidations(getClass().getMethod("twoValidations",new Class<?>[0]));
		
		assertEquals("number of validations",2,validations.size());
	}
	
	@Test
	public void testGetValidations_WithNoValidations() throws NoSuchMethodException, SecurityException {
		
		List<Annotation> validations = Validation.getValidations(getClass().getMethod("noValidations",new Class<?>[0]));
		
		assertEquals("number of validations",0,validations.size());
	}
	
	@Test
	public void testValidate_AllwaysPasses() throws NoSuchMethodException, SecurityException{
		
		List<String> result;
		result = Validation.validate(getClass().getMethod("allwaysPasses",new Class<?>[0]), allwaysPasses());
		
		assertNull("result",result);
	}
	
	@Test
	public void testValidate_AllwaysFails() throws NoSuchMethodException, SecurityException{
		List<String> expected = new ArrayList<String>();
		expected.add("allways fails");
		
		List<String> result;
		result = Validation.validate(getClass().getMethod("allwaysFails",new Class<?>[0]), allwaysFails());
		
		assertNotNull("result",result);
		assertArrayEquals("",expected.toArray(),result.toArray());
	}
	
	@Test
	public void testValidate_MultipleMethods() throws NoSuchMethodException, SecurityException{
		List<String> expected = new ArrayList<String>();
		expected.add("allways fails");
		expected.add("allways fails");
		
		List<Method> methods = new ArrayList<Method>();
		methods.add(getClass().getMethod("allwaysFails",new Class<?>[0]));
		methods.add(getClass().getMethod("allwaysPasses",new Class<?>[0]));
		methods.add(getClass().getMethod("noValidations",new Class<?>[0]));
		methods.add(getClass().getMethod("twoValidations",new Class<?>[0]));
		
		List<String> result;
		result = Validation.validate(methods, allwaysFails());
		
		assertNotNull("result",result);
		assertArrayEquals("",expected.toArray(),result.toArray());
	}
	


	@Validation.ValidationAttribute(AllwaysPasses.Evaluator.class)
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public @interface AllwaysPasses{
		
		
		public class Evaluator extends ValidationEvaluatorBase {

			@Override
			public <V, A> String validate(V value, A attribute) {
				return null;
			}
		
		}
	}
	
	@Validation.ValidationAttribute(AllwaysFails.Evaluator.class)
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public @interface AllwaysFails{
		
		
		public class Evaluator extends ValidationEvaluatorBase {

			@Override
			public <V, A> String validate(V value, A attribute) {
				return "allways fails";
			}
		
		}
	}

}
