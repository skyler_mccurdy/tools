﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Validation
{
    public class Required:Validation
    {

        public string reason { get; protected set; }


        public Required()
        {
            reason = "Is required";
        }

        public Required(string reason)
        {
            this.reason = reason;
        }

        public override bool validate(object value, out string reason)
        {
            if (value == null)
            {
                reason = this.reason;
                return false;
            }

            if (value.ToString().Length == 0)
            {
                reason = this.reason;
                return false;
            }

            reason = string.Empty;
            return true;

        }
    }
}
