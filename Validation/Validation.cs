﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Validation
{
    public abstract class Validation : Attribute
    {

        public abstract bool validate(object value, out string reason);


        public static bool runValidations(object obj, PropertyInfo property, out string reason)
        {

            IEnumerable<Validation> validations = property.GetCustomAttributes<Validation>();

            object value = property.GetValue(obj, null);

            foreach (Validation v in validations)
            {
                if (!v.validate(value, out reason))
                {
                    return false;
                }
            }

            reason = string.Empty;
            return true;

        }
    }
}
