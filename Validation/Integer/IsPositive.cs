﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Validation.Integer
{
    public class IsPositive:Validation
    {
        public override bool validate(object value, out string reason)
        {
            try
            {
                int tmp = (int)value;
                if (tmp > 0)
                {
                    reason = string.Empty;
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            reason = "must be greater than zero";
            return false;

        }
    }
}
