﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Validation.Integer
{
    public class GreaterThan:Validation
    {
        public int value { get; set; }

        public GreaterThan(int value)
        {
            this.value = value;
        }

        public override bool validate(object obj, out string reason)
        {
            try
            {
                int tmp = (int)obj;
                if (tmp > value)
                {
                    reason = string.Empty;
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            reason = "must be greater than "+value;
            return false;

        }
    }
}
