﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Validation.Integer
{
    public class GreaterThanOrEqualTo:Validation
    {
        public int value { get; set; }

        public GreaterThanOrEqualTo(int value)
        {
            this.value = value;
        }

        public override bool validate(object obj, out string reason)
        {
            try
            {
                int tmp = (int)obj;
                if (tmp >= value)
                {
                    reason = string.Empty;
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            reason = "must be greater than or equal to "+value;
            return false;

        }
    }
}
