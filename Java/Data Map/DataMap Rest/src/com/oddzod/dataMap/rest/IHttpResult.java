package com.oddzod.dataMap.rest;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

public interface IHttpResult {
	
	public boolean onSuccess(IHttpConnection connection,int resultCode,Map<String,String> responseHeaders, InputStream inputStream) throws IOException;
	
	public void onFailure(IHttpConnection connection,int resultCode,Map<String,String> responseHeaders, InputStream inputStream);

}
