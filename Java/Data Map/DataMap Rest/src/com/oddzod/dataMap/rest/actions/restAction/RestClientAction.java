package com.oddzod.dataMap.rest.actions.restAction;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.ClassUtils;

import com.google.gson.Gson;
import com.oddzod.dataMap.IDataFactory;
import com.oddzod.dataMap.IObject;
import com.oddzod.dataMap.rest.Property;
import com.oddzod.dataMap.rest.client.RestClientFactoryBase;



@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface RestClientAction {

	public Class<?> value();
	
	public interface IClientAction{
		public void execute(IObject obj,Annotation cmd)  throws IllegalAccessException, MalformedURLException, IOException;

	}
	
	public static abstract class ClientAction implements IClientAction{
		
		protected List<Annotation> getParameters(Class<? extends Annotation> annotationType){
			
			return null;
		}
		
		
		

		protected URL buildURL(IDataFactory ifactory,String url,Map<String,Object> parameters) throws MalformedURLException{
			RestClientFactoryBase factory = (RestClientFactoryBase)ifactory; 

			if(parameters!=null){
				for(String param : parameters.keySet()){
					url = url.replaceAll("\\{"+param+"\\}", parameters.get(param).toString());
				}
			}
			
			if(url.matches("[^\\{]*\\{[^\\}]*\\}.*")){
				Pattern rx = Pattern.compile("\\{[^\\}]*\\}");
				Matcher m = rx.matcher(url);
				m.find();
				
				throw new IllegalArgumentException("Parameter "+m.group()+" Not provided");
			}
			
			return new URL(factory.getBaseURL() + url);
			
		}
		
		
		protected <T extends Annotation> Map<String,Object> getParameters(IObject obj,Class<T> parameterType)  throws IllegalAccessException{
			Map<String,Object> params = new HashMap<String,Object>();
			
			for(Method method : obj.getClass().getMethods()){
				T parameter = method.getAnnotation(parameterType);
				if(parameter != null){
					String name = null;
					Object value = null;
					try{
						name = parameter.getClass().getMethod("value").invoke(parameter, (Object[])null).toString();
						value = method.invoke(obj, (Object[])null);
					}catch(Exception ex){
						throw new IllegalAccessException("Unable to access value for "+obj.getClass().getName()+"."+method.getName());
					}
					params.put(name,value);
				}
			}
			
			return params;
		}
		
		@SuppressWarnings("unchecked")
		protected ArrayList<Map<String,Object>> parseJSONArray(String json){
			ArrayList<HashMap<String,Object>> result = new ArrayList<HashMap<String,Object>>();
			
			
			Gson gson = new Gson();		
			
			return (ArrayList<Map<String,Object>>) gson.fromJson(json, result.getClass());
		}
		
		@SuppressWarnings("unchecked")
		protected Map<String,Object> parseJSON(String json){
			HashMap<String,Object> result = new HashMap<String,Object>();
			
			
			Gson gson = new Gson();		
			
			return (HashMap<String,Object>) gson.fromJson(json, result.getClass());
		}
		
		protected String convertStreamToString(java.io.InputStream is) {
		    java.util.Scanner s=null;
		    try{
			    s = new java.util.Scanner(is).useDelimiter("\\A");
			    return s.hasNext() ? s.next() : "";
		    }finally{
		    	if(s!=null)
		    		s.close();
		    }
		}
		
		protected void setPropertiesFrom(IObject obj,Map<String,Object> source) throws IllegalAccessException, IOException, IllegalArgumentException, InvocationTargetException{
			Map<String,PropertyMap> properties = getProperties(obj);
			
			for(String propertyName : properties.keySet()){
				PropertyMap map = properties.get(propertyName);
				if(map.setters.size()>0){
					if(source.containsKey(propertyName)){
						
						if(source.get(propertyName)!=null){
							Object value = source.get(propertyName);
							
							Method setter = map.getSetter(source.get(propertyName).getClass());
							
							if(ClassUtils.isAssignable(value.getClass(), Double.class,true) || ClassUtils.isAssignable(value.getClass(), Long.class,true)){
								
								if(setter.getParameterTypes()[0] == int.class || setter.getParameterTypes()[0] == Integer.class){
									value = ((Double) value).intValue();
								}else{
									if(setter.getParameterTypes()[0] == long.class || setter.getParameterTypes()[0] == Long.class){
										value = ((Double) value).longValue();
									}
								}
							}else if(Date.class.isAssignableFrom(map.setters.get(0).getParameterTypes()[0])){
								DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
								try {
									value =  df.parse((String)value);
								} catch (ParseException e) {
									e.toString();
								}  
							}
							
							map.setters.get(0).invoke(obj, value);
						}else{
							map.setters.get(0).invoke(obj, new Object[]{null});
						}
						
						
					}else{
						throw new IOException(propertyName+" not found in source");
					}
				}
			}
			
		}
		
		protected static class PropertyMap{
			public String name;
			public List<Method> getters = new ArrayList<Method>();
			public List<Method> setters = new ArrayList<Method>();
			
			public Method getSetter(Class<?> paramType){
				for(Method setter : setters){
					Class<?>[] paramTypes = setter.getParameterTypes();
					if(paramTypes.length == 1){
						if(ClassUtils.isAssignable(paramType, paramTypes[0],true)){
							return setter;
						}else{
							if(ClassUtils.isAssignable(paramType, Double.class,true)
									|| ClassUtils.isAssignable(paramType, Long.class,true)){
								if(ClassUtils.isAssignable(paramTypes[0], Long.class,true)
										|| ClassUtils.isAssignable(paramTypes[0], Integer.class,true)){
									return setter;
								}
							}
						}
					}
				}
				return null;
			}
		}
		
		protected Map<String,PropertyMap> getProperties(IObject obj)  throws IllegalAccessException{
			Map<String,PropertyMap> properties = new HashMap<String,PropertyMap>();
			
			for(Method method : obj.getClass().getMethods()){
				Property.Get get = method.getAnnotation(Property.Get.class);
				if(get != null){
					PropertyMap map;
					if(properties.containsKey(get.name())){
						map = properties.get(get.name());
					}else{
						map = new PropertyMap();
						map.name = get.name();
					}
					map.getters.add(method);
					properties.put(get.name(), map);
				}
				
				Property.Set set = method.getAnnotation(Property.Set.class);
				if(set != null){
					PropertyMap map;
					if(properties.containsKey(set.name())){
						map = properties.get(set.name());
					}else{
						map = new PropertyMap();
						map.name = set.name();
					}
					map.setters.add(method);
					properties.put(set.name(), map);
				}
			}
			
			return properties;
		}

		
		
	}
}
