package com.oddzod.dataMap.rest.client;

import com.oddzod.dataMap.IDataInterface;
import com.oddzod.dataMap.IObject;
import com.oddzod.dataMap.baseType.DataFactoryBase;
import com.oddzod.dataMap.rest.IHttpConnection;

public abstract class RestClientFactory extends DataFactoryBase {

	public RestClientFactory() {
		super();
	}

	@Override
	public IDataInterface getDataInterface(IObject obj) {
		return new DataInterface(obj);
	}
	
	public abstract IHttpConnection getConnection();

}
