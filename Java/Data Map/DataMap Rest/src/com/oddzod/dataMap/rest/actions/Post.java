package com.oddzod.dataMap.rest.actions;

import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.Map;

import com.oddzod.dataMap.IObject;
import com.oddzod.dataMap.rest.IHttpConnection;
import com.oddzod.dataMap.rest.IHttpResult;
import com.oddzod.dataMap.rest.actions.restAction.RestClientAction;
import com.oddzod.dataMap.rest.client.RestClientFactoryBase;


@RestClientAction(Post.PostAction.class)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Post {

	
	public String value();
	
	
	@Retention(RetentionPolicy.RUNTIME)
	public @interface Parameter{
		public String value();
	}
	
	public static class PostAction extends RestClientAction.ClientAction{
		
		public void execute(IObject obj,Annotation annotation) throws IllegalAccessException, IOException{
			
			Post cmd = (Post) annotation;
			Map<String,Object> params = getParameters(obj,Post.Parameter.class);
			URL url = buildURL(obj.getFactory(),cmd.value(),params);
			
			RestClientFactoryBase factory = (RestClientFactoryBase)obj.getFactory();
			
			IHttpConnection con = factory.getConnection();
			con.setURL(url.toString());
			con.setMethod(IHttpConnection.POST);
			con.setHttpResult(new RestCallback(obj));
			
			Map<String,PropertyMap> properties = getProperties(obj);
			for(String propertyName : properties.keySet()){
				PropertyMap prop = properties.get(propertyName);
				
				if(prop.getters.size() > 0){
					try{
						Object value = prop.getters.get(0).invoke(obj, (Object[])null);
						if(value!=null){
							con.setParameter(prop.name, value.toString());
						}
					}catch(InvocationTargetException ex){
						throw new IllegalAccessException("Unable to get value of "+obj.getClass().getName()+"."+prop.getters.get(0).getName());
					}
				}
			}
			
			if(!con.connect()){
				throw new IOException("Unable to post resource to "+url.toString());
			}
		}
		
		protected class RestCallback implements IHttpResult{

			protected IObject obj;
			
			public RestCallback(IObject obj){
				this.obj=obj;
			}
			
			@Override
			public boolean onSuccess(IHttpConnection connection,
					int resultCode, Map<String, String> responseHeaders,
					InputStream inputStream) throws IOException {

				String json = convertStreamToString(inputStream);
				Map<String,Object> result = parseJSON(json);
				
				try {
					
					setPropertiesFrom(obj,result);
					
				} catch (IllegalAccessException e) {
					throw new IOException("IllegalAccessException",e);
				} catch (IllegalArgumentException e) {
					throw new IOException("IllegalArgumentException",e);
				} catch (InvocationTargetException e) {
					throw new IOException("InvocationTargetException",e);
				}
				
				return true;
			}

			@Override
			public void onFailure(IHttpConnection connection, int resultCode,
					Map<String, String> responseHeaders, InputStream inputStream) {

				
			}
			
			
			
		}
	}
	
	
}
