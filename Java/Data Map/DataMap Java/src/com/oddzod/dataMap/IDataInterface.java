package com.oddzod.dataMap;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

public interface IDataInterface {
	
	/**
	 * Reloads object for data store
	 * 
	 * @return true if successful
	 * @throws IOException
	 * @throws IllegalAccessException 
	 */
	public boolean refresh() throws IOException, IllegalAccessException;
	
	/**
	 * Commits the object to the data store
	 * 
	 * @return true if successful
	 * @throws IOException
	 * @throws IllegalAccessException 
	 */
	public boolean commit() throws IOException, IllegalAccessException;
	
	/**
	 * Deletes the object from the data store
	 * 
	 * @return true if successful
	 * @throws IOException
	 * @throws IllegalAccessException 
	 */
	public boolean delete() throws IOException, IllegalAccessException;
	
	
	/**
	 * Retrieves an array of specified type using the annotation provided on the supplied method
	 * 
	 * @param type the type of object to return
	 * @param method the method that called this
	 * @return
	 * @throws IOException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 */
	public <T extends IObject> T[] getArray(Class<T> type,Method method) throws IllegalAccessException, IOException, InstantiationException;
	
	/**
	 * Flags a data element as changed
	 * 
	 * @param name
	 */
	public void changed(String name);
	
	/**
	 * Gets the IDataFactory that created this object
	 * 
	 * @return
	 */
	public IDataFactory getFactory();
	
	/**
	 * Sets the IDataFactory that created this object
	 * 
	 * Throws an IllegalAccessException if the factory has already been set
	 * 
	 * @param factory the factory that created this object
	 * @throws IllegalAccessException
	 */
	public void setFactory(IDataFactory factory) throws IllegalAccessException;
	
	/**
	 * Gets the object that is associated with this object
	 * 
	 * @return
	 */
	public IObject getObject();
	
	/**
	 * Sets the object that is associated with this object
	 * 
	 * Throws an IllegalAccessException if the object has already been set
	 * 
	 * @param obj
	 * @throws IllegalAccessException
	 */
	public void setObject(IObject obj) throws IllegalAccessException;
	
	/**
	 * Validates the value for the given element
	 * 
	 * Returns null if all validations passed
	 * 
	 * @param name name of element
	 * @param value value to validate
	 * @return
	 */
	public List<String> validate(String name,Object value) throws NoSuchMethodException;
	
	/**
	 * Validates all elements on object
	 * 
	 * Returns null if all validations passed
	 * @return
	 */
	public Map<String,List<String>> validateAll();


	/**
	 * Finds the getter method for the given element
	 * 
	 * @param elementName name of element
	 * @return
	 * @throws NoSuchMethodException
	 */
	public Method getGetterForElement(String elementName) throws NoSuchMethodException;
	
	/**
	 * Finds the setter method for the given element
	 * 
	 * @param elementName name of element
	 * @return
	 * @throws NoSuchMethodException
	 */
	public Method getSetterForElement(String elementName) throws NoSuchMethodException;
}
