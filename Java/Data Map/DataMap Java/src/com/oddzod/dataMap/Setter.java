package com.oddzod.dataMap;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks a method as setter for a data element
 * 
 * @author oddzod
 * @param value name of element
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Setter {

	/**
	 * name of element
	 * 
	 * @return
	 */
	String value();
}