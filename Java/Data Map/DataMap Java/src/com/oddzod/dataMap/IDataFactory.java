package com.oddzod.dataMap;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


/**
 * IObject factory base class
 * 
 * @author oddzod
 *
 */
public interface IDataFactory {

	
	/**
	 * Creates a new IObject for the specified type
	 * 
	 * create does not use caching. Each call to create, creates a new instance
	 * 
	 * @param type The type of IObject to create
	 * @return a new IObject instance of the type specified
	 * 
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 * @throws InvocationTargetException
	 * @throws IOException
	 */
	public IObject create(Class<?> type) throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException, InvocationTargetException , IOException;
	
	/**
	 * Creates a new IObject for the specified type
	 * 
	 * create does not use caching. Each call to create, creates a new instance
	 * 
	 * @param type The type of IObject to create
	 * @param arguments Constructor arguments for the IObject to be created
	 * @return a new IObject instance of the type specified
	 * 
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 * @throws InvocationTargetException
	 * @throws IOException
	 */
	public IObject create(Class<?> type,Object...arguments) throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException, InvocationTargetException, IOException;
	
	
	/**
	 * Gets an instance of the specified type
	 * 
	 * Uses caching. If an instance of the specified type, with the same unique identifier exists, a reference to that object will be returned.
	 * If an instance does not exist a new object will be created.
	 * 
	 * @param type The IObject type to create
	 * @param uniqueIdentifier the unique identifier to retrieve
	 * @return an IObject instance
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 * @throws InvocationTargetException
	 * @throws IOException
	 */
	public IObject get(Class<?> type,long uniqueIdentifier) throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException, InvocationTargetException, IOException;
	
	
	/**
	 * Retrieves an array of specified type using the annotation provided on the supplied method
	 * 
	 * @param type the type of object to return
	 * @param method the method that called this
	 * @return
	 * @throws IOException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 */
	public <T extends IObject> T[] getArray(Class<T> type,Method method) throws IllegalAccessException, IOException, InstantiationException;

	/**
	 * Internal use only.
	 * 
	 * Creates a new IDataInterface object for use with the specified IObject
	 * 
	 * @param obj
	 * @return
	 */
	public abstract IDataInterface getDataInterface(IObject obj);
}
