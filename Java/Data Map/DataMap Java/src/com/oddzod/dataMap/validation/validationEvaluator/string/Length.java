package com.oddzod.dataMap.validation.validationEvaluator.string;

import com.oddzod.dataMap.validation.validationEvaluator.ValidationEvaluatorBase;

public class Length extends ValidationEvaluatorBase {

	//@Override
	public <V, A> String validate(V value, A attribute) {
		
		com.oddzod.dataMap.validation.string.Length attrib;
		String strValue;
		
		try{
			attrib  = (com.oddzod.dataMap.validation.string.Length) attribute;
		}catch(ClassCastException ex){
			throw new ClassCastException("attribute not of type "+com.oddzod.dataMap.validation.string.Length.class.getName());
		}
		
		try{
			strValue = (String) value;
		}catch(ClassCastException ex){
			throw new ClassCastException("must be a String");
		}
		
		if(strValue == null)
			return null;
		
		if(attrib.min() >= 0 && strValue.length() < attrib.min())
			return "must be at least "+attrib.min()+" characters long";
		
		if(attrib.max() >= 0 && strValue.length() > attrib.max())
			return "must be at most "+attrib.max()+" characters long";
		
		
		return null;
	}

	

}
