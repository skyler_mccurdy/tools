package com.oddzod.dataMap.validation;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import com.oddzod.dataMap.validation.validationEvaluator.ValidationEvaluatorBase;



public class Validation{
	
	/**
	 * Returns a list of validations for a method
	 * 
	 * @param method
	 * @return
	 */
	public static List<Annotation> getValidations(Method method){
		
		List<Annotation> annotations = new ArrayList<Annotation>();
		
		for(Annotation annotation : method.getAnnotations()){
			
			ValidationAttribute valAtt = annotation.annotationType().getAnnotation(Validation.ValidationAttribute.class);
			if(valAtt!=null){
				annotations.add(annotation);
			}
		}
		
		return annotations;
		
	}
	
	
	/**
	 * Runs all evaluations for method given
	 * 
	 * @param method
	 * @param value
	 * @return
	 */
	public static List<String> validate(Method method,Object value) {
	
		List<Method> methods = new ArrayList<Method>();
		methods.add(method);
		
		return validate(methods,value);
		
	}
	
	/**
	 * Runs all evaluations for methods given
	 * 
	 * Returns null if all passed. If any fail, a List<String> of error messages are returned
	 * 
	 * @param methods
	 * @param value
	 * @return
	 */
	public static List<String> validate(List<Method> methods,Object value){
		List<String> errors = new ArrayList<String>();
		
		for(Method method : methods){
			for(Annotation annotation : getValidations(method)){
				ValidationAttribute validation = annotation.annotationType().getAnnotation(Validation.ValidationAttribute.class);
				ValidationEvaluatorBase evaluator=null;
				try {
					evaluator = (ValidationEvaluatorBase) validation.value().newInstance();
				} catch (InstantiationException e){
					
				} catch( IllegalAccessException e) {
					throw new RuntimeException("unable to instantiate "+validation.value().getName());
				}
				
				String result = evaluator.validate(value, annotation);
				
				if(result!=null){
					errors.add(result);
				}
			}
		}
		
		return (errors.size()>0)?errors:null;
	}
	
	
	/**
	 * Marks an annotation as being a validation annotation
	 * 
	 * @author oddzod
	 *
	 */
	@Target(ElementType.ANNOTATION_TYPE)
	@Retention(RetentionPolicy.RUNTIME)
	public @interface ValidationAttribute {

		Class<?> value();
		
	}
	
	public static interface IValidationEvaluator{
		
		public <V,A> String validate(V value,A attribute);
	}
}
