package com.oddzod.dataMap.validation.string;

import com.oddzod.dataMap.validation.Validation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;


@Validation.ValidationAttribute(com.oddzod.dataMap.validation.validationEvaluator.string.Length.class)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Length{

	int min() default -1;
	int max() default -1;
	
}
