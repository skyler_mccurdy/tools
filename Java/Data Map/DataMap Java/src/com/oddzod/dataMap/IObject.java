package com.oddzod.dataMap;

import java.io.IOException;

/**
 * A Data Map data object
 * 
 * @author oddzod
 *
 */
public interface IObject {

	/**
	 * Reloads the object from the data store
	 * 
	 * 
	 * Will call getFactory.addToCache with the objects current uniqueIdentifier
	 * 
	 * @return true if successful
	 * @throws IOException
	 * @throws IllegalAccessException 
	 */
	public boolean refresh() throws IOException, IllegalAccessException;
	
	/**
	 * Commits the object to the data store
	 * 
	 * Will call getFactory.addToCache with the objects current uniqueIdentifier
	 * 
	 * @return true if successful
	 * @throws IOException
	 * @throws IllegalAccessException 
	 */
	public boolean commit() throws IOException, IllegalAccessException;
	
	/**
	 * Deletes the object from the data store
	 * 
	 * @return true if successful
	 * @throws IOException
	 * @throws IllegalAccessException 
	 */
	public boolean delete() throws IOException, IllegalAccessException;
	
	/**
	 * returns true if the object has never been saved
	 * 
	 * @return
	 */
	public boolean isNew();
	
	/**
	 * returns true if the object has been modified since the last call to refresh or commit
	 * 
	 * @return
	 */
	public boolean isModified();
	
	/**
	 * Flags a data element has having been changed
	 * 
	 * @param name
	 */
	public void changed(String name);
	
	
	/**
	 * Returns the IDataFactory that created this object
	 * 
	 * @return
	 */
	public IDataFactory getFactory();
	
	/**
	 * Sets the IDataFactory that created this object
	 * 
	 * Throws IllegalAccesException if the factory has already been set
	 * 
	 * @param factory
	 * @throws IllegalAccessException
	 */
	public void setFactory(IDataFactory factory) throws IllegalAccessException;
	
	/**
	 * Gets the IDataInterface associated with this object
	 * 
	 * @return
	 */
	public IDataInterface getDataInterface();
	
	/**
	 * Sets the IDataInterface associated with this object
	 * 
	 * @param dataInterface
	 * @throws IllegalAccessException
	 */
	public void setDataInterface(IDataInterface dataInterface) throws IllegalAccessException;
	
	/**
	 * Returns a unique identifier for this object
	 * @return
	 */
	public long getUniqueIdentifier();
	
}
