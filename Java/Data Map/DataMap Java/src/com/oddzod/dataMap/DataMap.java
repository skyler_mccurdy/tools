package com.oddzod.dataMap;

import java.util.HashMap;

/**
 * Primary entry point for Data Map.
 * 
 * An IDataFactory factory ;)
 * 
 * @author oddzod
 *
 */
public class DataMap {
	
	/**
	 * stores instances of the factories that have been created.
	 */
	private static HashMap<String,IDataFactory> factories = new HashMap<String,IDataFactory>();

	/**
	 * Data Map version number as hex
	 */
	private static int version = 0x000100;
	

	/**
	 * Gets or creates an instance of the specified type
	 * 
	 * @param factoryClass IDataFactory implementation to get/create
	 * @return an instance of the specified IDataFactory
	 * 
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 */
	public static IDataFactory getFactory(Class<?> factoryClass) throws InstantiationException, IllegalAccessException, IllegalArgumentException{
		
		if(!IDataFactory.class.isAssignableFrom(factoryClass)){
			throw new IllegalArgumentException(factoryClass.getName() + " does not implement IDataFactory interface.");
		}
		
		String factoryName = factoryClass.getName();
		
		if(!factories.containsKey(factoryName)){
			factories.put(factoryName, (IDataFactory) factoryClass.newInstance());
		}
		
		return factories.get(factoryName);
	}
	
	/**
	 * Returns the version of Data Map as a hex number
	 * @return
	 */
	public static int getVersion(){
		return version;
	}
	
	/**
	 * Returns the major version of Data Map
	 * @return
	 */
	public int getMajorVersion(){
		return version & 0xFF0000 >> 16;
	}
	
	/**
	 * Returns the minor version of Data Map
	 * @return
	 */
	public int getMinorVersion(){
		return version & 0xFF00 >> 8;
	}
	
	/**
	 * returns the patch version of Data Map
	 * @return
	 */
	public int getPatchVersion(){
		return version & 0xFF;
	}
	
	/**
	 * Returns the string representation of Data Map
	 * @return
	 */
	public String getVersionString(){
		return "Data Map "+getMajorVersion()+"."+getMinorVersion()+"."+getPatchVersion();
	}
	
}
