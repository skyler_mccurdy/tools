package com.oddzod.dataMap.baseType;

import java.io.IOException;

import com.oddzod.dataMap.IDataFactory;
import com.oddzod.dataMap.IDataInterface;
import com.oddzod.dataMap.IObject;

/**
 * Base class for IObject objects
 * 
 * Implements most common behavior
 * 
 * @author oddzod
 *
 */
public abstract class ObjectBase implements IObject {
	
	private IDataInterface dataInterface = null;
	private IDataFactory dataFactory = null;
	
	protected boolean _isNew;
	protected boolean _isModified;
	
	
	/**
	 * Creates a new object
	 */
	public ObjectBase(){
		setNew(true);
		setModified(true);
	}
	
	public ObjectBase(Long uniqueIdentifier) throws IOException{
		setNew(false);
		setModified(true);
		setUniqueIdentifier(uniqueIdentifier);
		
	}

	/**
	 * Reloads the object from the data store
	 * 
	 * 
	 * Will call getFactory.addToCache with the objects current uniqueIdentifier
	 * 
	 * @return true if successful
	 * @throws IOException
	 * @throws IllegalAccessException 
	 */

	@Override
	public boolean refresh() throws IOException, IllegalAccessException {
		if(getDataInterface().refresh()){
			setNew(false);
			setModified(false);
			return true;
		}
		return false;
	}

	/**
	 * Commits the object to the data store
	 * 
	 * Will call getFactory.addToCache with the objects current uniqueIdentifier
	 * 
	 * @return true if successful
	 * @throws IOException
	 * @throws IllegalAccessException 
	 */

	@Override
	public boolean commit() throws IOException, IllegalAccessException {

		if(getDataInterface().commit()){
			setNew(false);
			setModified(false);
			return true;
		}
		return false;
	}

	/**
	 * Deletes the object from the data store
	 * 
	 * @return true if successful
	 * @throws IOException
	 * @throws IllegalAccessException 
	 */

	@Override
	public boolean delete() throws IOException, IllegalAccessException {

		return getDataInterface().delete();
	}

	/**
	 * returns true if the object has never been saved
	 * 
	 * @return
	 */
	//@Override
	public boolean isNew() {
		return _isNew;
	}
	
	/**
	 * Sets the isNew field
	 * 
	 * @param isNew
	 */
	protected void setNew(boolean isNew){
		this._isNew=isNew;
	}

	/**
	 * returns true if the object has been modified since the last call to refresh or commit
	 * 
	 * @return
	 */
	//@Override
	public boolean isModified() {
		return _isModified;
	}
	
	/**
	 * 
	 * @param isModified
	 */
	protected void setModified(boolean isModified){
		this._isModified=isModified;
	}

	/**
	 * Flags a data element has having been changed
	 * 
	 * @param name
	 */
	//@Override
	public void changed(String name) {
		if(getDataInterface()!=null)
			getDataInterface().changed(name);
	}

	/**
	 * Returns the IDataFactory that created this object
	 * 
	 * @return
	 */
	//@Override
	public IDataFactory getFactory() {
		return dataFactory;
	}

	/**
	 * Sets the IDataFactory that created this object
	 * 
	 * Throws IllegalAccesException if the factory has already been set
	 * 
	 * @param factory
	 * @throws IllegalAccessException
	 */
	//@Override
	public void setFactory(IDataFactory factory) throws IllegalAccessException {
		if(dataFactory != null){
			throw new IllegalAccessException("setFactory can only be called once");
		}
		this.dataFactory=factory;
	}

	/**
	 * Gets the IDataInterface associated with this object
	 * 
	 * @return
	 */
	//@Override
	public IDataInterface getDataInterface() {
		return dataInterface;
	}

	/**
	 * Sets the IDataInterface associated with this object
	 * 
	 * @param dataInterface
	 * @throws IllegalAccessException
	 */
	//@Override
	public void setDataInterface(IDataInterface dataInterface)
			throws IllegalAccessException {
		if(this.dataInterface!=null){
			throw new IllegalAccessException("setInterface can only be called once");
		}
		
		this.dataInterface=dataInterface;
	}

	/**
	 * Sets the objects unique identifier
	 * 
	 * @param uniqueIdentifier
	 */
	protected abstract void setUniqueIdentifier(long uniqueIdentifier);

}
