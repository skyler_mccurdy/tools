package com.oddzod.dataMap.baseType;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import com.oddzod.dataMap.Getter;
import com.oddzod.dataMap.IDataFactory;
import com.oddzod.dataMap.IDataInterface;
import com.oddzod.dataMap.IObject;
import com.oddzod.dataMap.Setter;
import com.oddzod.dataMap.validation.Validation;


public abstract class DataInterfaceBase implements IDataInterface {

	/**
	 * The factory that created this data interface
	 */
	private IDataFactory dataFactory=null;
	
	/**
	 * the object that this interface is associated with
	 */
	private IObject object =null;
	
	/**
	 * Validates the value for the given element
	 * 
	 * Returns null if all validations passes
	 * 
	 * @param name name of element
	 * @param value value to validate
	 * @return
	 * @throws NoSuchMethodException 
	 */
	public List<String> validate(String name,Object value) throws NoSuchMethodException{
		Method method = getGetterForElement(name);
		
		return Validation.validate(method, value);
		
	}
	
	/**
	 * Validates all elements on object
	 * 
	 * Returns null if all validations passed
	 * @return
	 */
	public Map<String,List<String>> validateAll(){
		Map<String,List<String>> errors = new HashMap<String,List<String>>();
		
		for(Method method : getObject().getClass().getMethods()){
			Getter getter = method.getAnnotation(Getter.class);
			if(getter!=null){
				String elementName = getter.value();
				Object value;
				try {
					value = method.invoke(getObject());
					
					List<String> result;
					
					result = validate(elementName,value);
					
					if(result!=null){
						if(!errors.containsKey(elementName)){
							errors.put(elementName,new ArrayList<String>());
						}
						errors.get(elementName).addAll(result);
					}
					
				} catch (Exception e) {
					if(!errors.containsKey(elementName)){
						errors.put(elementName,new ArrayList<String>());
					}
					errors.get(elementName).add("exception occured while validate");
				}
			}
		}
		
		return (errors.keySet().size()>0)?errors:null;
	}
	
	/**
	 * Finds the getter method for the given element
	 * 
	 * @param elementName name of element
	 * @return
	 * @throws NoSuchMethodException
	 */
	public Method getGetterForElement(String elementName) throws NoSuchMethodException{
		
		for(Method method : getObject().getClass().getMethods()){
			Getter getter = method.getAnnotation(Getter.class);
			if(getter!=null && getter.value().equals(elementName)){
				return method;
			}
		}
		
		throw new NoSuchMethodException("not getter for "+elementName+" found");
	}
	
	/**
	 * Finds the setter method for the given element
	 * 
	 * @param elementName name of element
	 * @return
	 * @throws NoSuchMethodException
	 */
	public Method getSetterForElement(String elementName) throws NoSuchMethodException{
		
		for(Method method : getObject().getClass().getMethods()){
			Setter setter = method.getAnnotation(Setter.class);
			if(setter!=null && setter.value().equals(elementName)){
				return method;
			}
		}
		
		throw new NoSuchMethodException("not setter for "+elementName+" found");
	}
	
	/**
	 * Gets the IDataFactory that created this object
	 * 
	 * @return
	 */
	public IDataFactory getFactory(){
		return dataFactory;
	}
	
	/**
	 * Sets the IDataFactory that created this object
	 * 
	 * Throws an IllegalAccessException if the factory has already been set
	 * 
	 * @param factory the factory that created this object
	 * @throws IllegalAccessException
	 */
	public void setFactory(IDataFactory factory) throws IllegalAccessException{
		if(dataFactory!=null){
			throw new IllegalAccessException("setFactory can only be called once");
		}
		this.dataFactory=factory;
	}
	
	/**
	 * Gets the object that is associated with this object
	 * 
	 * @return
	 */
	public IObject getObject(){
		return object;
	}
	
	/**
	 * Sets the object that is associated with this object
	 * 
	 * Throws an IllegalAccessException if the object has already been set
	 * 
	 * @param obj
	 * @throws IllegalAccessException
	 */
	public void setObject(IObject obj) throws IllegalAccessException{
		if(object!=null){
			throw new IllegalAccessException("setObject can only be called once");
		}
		this.object=obj;
	}

}
