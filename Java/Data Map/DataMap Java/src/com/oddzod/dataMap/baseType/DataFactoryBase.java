package com.oddzod.dataMap.baseType;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;


import com.oddzod.dataMap.IDataFactory;
import com.oddzod.dataMap.IObject;


/**
 * Base class for IDataFactory objects
 * 
 * Provides the most common functionality
 * 
 * @author oddzod
 *
 */
public abstract class DataFactoryBase implements IDataFactory {
	
	/**
	 * cache of objects created by this factory
	 * 
	 */
	private HashMap<String,HashMap<Long,WeakReference<IObject>>> objectCache = new HashMap<String,HashMap<Long,WeakReference<IObject>>>();

	
	/**
	 * Creates a new IObject for the specified type
	 * 
	 * create does not use caching. Each call to create, creates a new instance
	 * 
	 * @param type The type of IObject to create
	 * @return a new IObject instance of the type specified
	 * 
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 * @throws InvocationTargetException
	 * @throws IOException
	 */
	//@Override
	public IObject create(Class<?> type) throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException, InvocationTargetException {
		
		if(!IObject.class.isAssignableFrom(type)){
			throw new IllegalArgumentException(type.getName()+" does not implement IObject interface");
		}
		
		IObject obj = (IObject) type.newInstance();
		obj.setFactory(this);
		obj.setDataInterface(getDataInterface(obj));
		
		
		return obj;
	}
	
	
	/**
	 * Creates a new IObject for the specified type
	 * 
	 * create does not use caching. Each call to create, creates a new instance
	 * 
	 * @param type The type of IObject to create
	 * @param arguments Constructor arguments for the IObject to be created
	 * @return a new IObject instance of the type specified
	 * 
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 * @throws InvocationTargetException
	 * @throws IOException
	 */
	//@Override
	public IObject create(Class<?> type, Object... arguments) throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException, InvocationTargetException {
		
		if(!IObject.class.isAssignableFrom(type)){
			throw new IllegalArgumentException(type.getName()+" does not implement IObject interface");
		}
		
		Class<?>[] argumentTypes = new Class<?>[arguments.length];
		for(int i = 0;i<arguments.length;i++){
			argumentTypes[i] = arguments[i].getClass();
		}
		
		IObject obj = (IObject) type.getConstructor(argumentTypes).newInstance(arguments);
		
		obj.setFactory(this);
		obj.setDataInterface(getDataInterface(obj));
		
		return obj;
	}
	
	
	/**
	 * Gets an instance of the specified type
	 * 
	 * Uses caching. If an instance of the specified type, with the same unique identifier exists, a reference to that object will be returned.
	 * If an instance does not exist a new object will be created.
	 * 
	 * @param type The IObject type to create
	 * @param uniqueIdentifier the unique identifier to retrieve
	 * @return an IObject instance
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 * @throws InvocationTargetException
	 * @throws IOException
	 */
	//@Override
	public IObject get(Class<?> type, long uniqueIdentifier) throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException, InvocationTargetException, IOException {
		
		if(!IObject.class.isAssignableFrom(type)){
			throw new IllegalArgumentException(type.getName()+" does not implement IObject interface");
		}
		
		if(!objectCache.containsKey(type.getName())){
			objectCache.put(type.getName(),new HashMap<Long,WeakReference<IObject>>());
		}
		
		if(!objectCache.get(type.getName()).containsKey(uniqueIdentifier) || objectCache.get(type.getName()).get(uniqueIdentifier) == null){
			
			
			
			IObject obj = (IObject) type.getConstructor(Long.class).newInstance(uniqueIdentifier);
			obj.setFactory(this);
			obj.setDataInterface(getDataInterface(obj));
			if(!obj.refresh()){
				throw new InvocationTargetException(null, "unable to load "+type.getName()+"("+uniqueIdentifier+")");
			}
			
			objectCache.get(type.getName()).put(uniqueIdentifier, new WeakReference<IObject>(obj));
			
		}
		
		
		return objectCache.get(type.getName()).get(uniqueIdentifier).get();
	}

}
