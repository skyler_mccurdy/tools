package com.oddzod.dataMap.rest.client.httpDrive;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import com.oddzod.dataMap.rest.client.*;
import com.oddzod.dataMap.rest.IHttpConnection;
import com.oddzod.dataMap.rest.IHttpResult;


public class HttpConnection implements IHttpConnection {
	
	protected STATE state = STATE.UNINITALIZED;
	protected IHttpResult resultCallback = null;
	protected String url = null;
	protected String method = null;
	protected HashMap<String,String> headers = new HashMap<String,String>();
	public Map<String,String> parameters = new HashMap<String,String>();
	protected int timeout = 10000;
	
	protected RestClientFactoryBase factory = null;
		
	protected HttpURLConnection connection = null;
	
	
	public HttpConnection(RestClientFactoryBase factory){
		this.factory=factory;
	}
	
	@Override
	public STATE getState() {
		return state;
	}

	@Override
	public IHttpResult getHttpResult() {
		return resultCallback;
	}

	@Override
	public void setHttpResult(IHttpResult resultCallback) {
		this.resultCallback=resultCallback;
	}

	@Override
	public String getURL() {
		return url;
	}
	
	@Override
	public void setURL(String url) {
		
		this.url = url;
	}

	@Override
	public String getMethod() {
		return method;
	}

	@Override
	public void setMethod(String method) {
		this.method=method;
	}

	@Override
	public String getHeader(String name) {
		if(headers.containsKey(name))
			return headers.get(name);
		return null;
	}

	@Override
	public void setHeader(String name, String value) {
		headers.put(name, value);
	}
	
	@Override
	public String getParameter(String name) {
		if(parameters.containsKey(name)){
			return parameters.get(name);
		}
		return null;
	}

	@Override
	public void setParameter(String name, String value) {
		parameters.put(name,value);
	}

	@Override
	public int getTimeout() {
		return timeout;
	}

	@Override
	public void setTimeout(int timeout) {
		this.timeout=timeout;
	}

	

	@Override
	public void close() {
		connection = null;
	}

	
	@Override
	public boolean connect() throws IOException {
		
        try{
            connection = (HttpURLConnection) new URL(/*factory.getBaseURL() + */url).openConnection();
            connection.setReadTimeout(timeout);
            connection.setConnectTimeout(timeout);
            connection.setRequestMethod(method);
            connection.setUseCaches(false);
            connection.setDoInput(true);
            if(POST.equals(method) || PUT.equals(method)){
            	connection.setDoOutput(true);
            	//connection.setRequestProperty ("Content-Type", "application/x-www-form-urlencoded");
            	connection.setRequestProperty ("Content-Type", "application/json; charset=utf-8");
            	
            	OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
            	
            	boolean first = true;
            	wr.write("{");
            	for(String name : parameters.keySet()){
            		if(!first){
           			wr.write(",");
            		}
            		first=false;
            		wr.write("\""+URLEncoder.encode(name, "UTF-8") + "\":\"" + URLEncoder.encode(parameters.get(name), "UTF-8")+"\"");;
            	}
            	wr.write("}");
            	
            	wr.flush();
            }
            
            for(String key : headers.keySet()){
            	connection.setRequestProperty(key, headers.get(key));
            }
            
            connection.connect();

            if(connection.getResponseCode() >= 200 && connection.getResponseCode() < 400){
                if(resultCallback!=null){
                	return resultCallback.onSuccess(this, connection.getResponseCode(), new HashMap<String,String>(), connection.getInputStream());
                }
                return true;
            }else{
            	if(resultCallback!=null){
                	resultCallback.onFailure(this, connection.getResponseCode(), new HashMap<String,String>(), connection.getInputStream());
                }
                return false;
            }

        }catch (Exception ex){
            if(resultCallback != null){
            	resultCallback.onFailure(this, 400, new HashMap<String,String>(), null);
            }
            
            throw new IOException("An exception occured connecting to the server",ex);
        }
	}
	

}
