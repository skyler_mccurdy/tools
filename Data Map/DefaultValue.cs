﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataMap
{
    public class DefaultValue:Attribute
    {
        public object value { get; set; }

        public DefaultValue(object value)
        {
            this.value = value;
        }
    }
}
