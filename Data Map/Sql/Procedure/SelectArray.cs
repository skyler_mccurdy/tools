﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Reflection;

namespace DataMap.Sql.Procedure
{
    public class SelectArray : DataMap.Sql.Procedure.CommandBase, DataMap.Sql.Command.ISelectArray
    {
        public SelectArray(string command):
            base(command)
        { }

        public IEnumerable<T> executeArray<T>(global::System.Data.SqlClient.SqlConnection connection, global::DataMap.Sql.DataInterface dataInterface, string propertyName) where T : IObject
        {
            Type objType = typeof(T);
            System.Data.SqlClient.SqlCommand cmd = null;
            SqlDataReader rdr = null;
            List<T> list = new List<T>();

            try
            {
                cmd = buildCommand<Parameter>(connection, dataInterface);

                rdr = cmd.ExecuteReader();

                T obj = dataInterface.factory.create<T>();

                while (readRow(rdr, (DataInterface) obj.dataInterface) == 1)
                {
                    obj.isNew = false;
                    obj.isModified = false;
                    list.Add(obj);
                    obj = dataInterface.factory.create<T>();
                }

                return list;
                

            }
            catch (Exception e)
            {
                sqlLog.error("Loading database objects " + dataInterface.GetType().Name + "." + propertyName + " failed", e);

                throw e;
            }
            finally
            {
                if (rdr != null)
                {
                    rdr.Close();
                }

                if (cmd != null)
                {
                    cmd.Cancel();
                }
            }
        }

        public IEnumerable<T> executeArray<T>(Sql.SqlFactory factory, Type type, string propertyName) where T : IObject
        {
            Type objType = typeof(T);
            System.Data.SqlClient.SqlCommand cmd = null;
            SqlDataReader rdr = null;
            List<T> list = new List<T>();

            try
            {
                cmd = buildCommand<Parameter>(factory.connection);

                rdr = cmd.ExecuteReader();

                T obj = factory.create<T>();

                while (readRow(rdr, (DataInterface)obj.dataInterface) == 1)
                {
                    list.Add(obj);
                    obj = factory.create<T>();
                }

                return list;


            }
            catch (Exception e)
            {
                sqlLog.error("Loading database objects " + type.Name + "." + propertyName + " failed", e);

                throw e;
            }
            finally
            {
                if (rdr != null)
                {
                    rdr.Close();
                }

                if (cmd != null)
                {
                    cmd.Cancel();
                }
            }
        }

        public class Parameter : Sql.Procedure.CommandBase.ParameterBase
        {
            public Parameter(string name, System.Data.SqlDbType sqlType) :
                base(name, sqlType)
            {

            }
        }
    }
}
