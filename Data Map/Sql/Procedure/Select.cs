﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Reflection;

namespace DataMap.Sql.Procedure
{
    public class Select:DataMap.Sql.Procedure.CommandBase, DataMap.Sql.Command.ISelect
    {
        public Select(string command):
            base(command)
        { }


        

        

        public override int execute(SqlConnection connection,DataInterface obj)
        {
            System.Data.SqlClient.SqlCommand cmd = null;
            SqlDataReader rdr = null;
            int returnValue = -1;

            try
            {
                cmd = buildCommand<Parameter>(connection,obj);

                rdr = cmd.ExecuteReader();

                returnValue = readRow(rdr, obj);

                return returnValue;

            }
            catch (Exception e)
            {
                sqlLog.error("Refreshing database object " + obj.GetType().Name + " failed", e);

                throw e;
            }
            finally
            {
                if (rdr != null)
                {
                    rdr.Close();
                }

                if (cmd != null)
                {
                    cmd.Cancel();
                }
            }

        }

        public class Parameter : CommandBase.ParameterBase
        {
            public Parameter(string name,System.Data.SqlDbType sqlType) :
                base(name,sqlType)
            {

            }
        }

        
    }
}
