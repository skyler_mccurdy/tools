﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Reflection;

namespace DataMap.Sql.Procedure
{
    public class Delete:DataMap.Sql.Procedure.CommandBase, DataMap.Sql.Command.IDelete
    {
        public Delete(string command) :
            base(command)
        { }


        

        

        public override int execute(SqlConnection connection,DataInterface obj)
        {
            System.Data.SqlClient.SqlCommand cmd = null;
            SqlDataReader rdr = null;

            try
            {
                cmd = buildCommand<Parameter>(connection, obj);

                return cmd.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                sqlLog.error("Deleting database object " + obj.GetType().Name + " failed", e);

                throw e;
            }
            finally
            {
                if (rdr != null)
                {
                    rdr.Close();
                }

                if (cmd != null)
                {
                    cmd.Cancel();
                }
            }

        }


        public class Parameter : CommandBase.ParameterBase
        {
            public Parameter(string name, System.Data.SqlDbType sqlType) :
                base(name,sqlType)
            {

            }
        }
        
    }
}
