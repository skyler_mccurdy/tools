﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Reflection;

namespace DataMap.Sql.Procedure
{
    public class CommandBase:Attribute, DataMap.Sql.Command.ISqlCommand
    {
        protected static LogIt.ILog sqlLog = LogIt.LogIt.getLog("sql");

        public string command { get; set; }


        public CommandBase(string command)
        {

            this.command = command;
            

        }

        public virtual int execute(SqlConnection connection, DataInterface obj)
        {
            throw new NotImplementedException();
        }

        protected virtual System.Data.SqlClient.SqlCommand buildCommand<T>(System.Data.SqlClient.SqlConnection connection, DataInterface dataInterface = null)
        {
            System.Data.SqlClient.SqlCommand cmd = null;

            cmd = new System.Data.SqlClient.SqlCommand(command, connection);

            if (dataInterface != null)
            {
                IEnumerable<PropertyInfo> properties = dataInterface.obj.GetType().GetProperties();

                foreach (PropertyInfo prop in properties)
                {
                    IEnumerable<Attribute> attributes = prop.GetCustomAttributes();
                    foreach (Attribute att in attributes)
                    {
                        if (typeof(T).IsInstanceOfType(att))
                        {
                            ParameterBase paramAttrib = att as ParameterBase;



                            object value = prop.GetValue(dataInterface.obj);

                            if (value == null)
                            {
                                cmd.Parameters.Add(paramAttrib.name, paramAttrib.sqlType).Value =
                                    DBNull.Value;
                            }
                            else
                            {
                                cmd.Parameters.Add(paramAttrib.name, paramAttrib.sqlType).Value =
                                    value;
                            }

                        }
                    }

                }
            }


            return cmd;

        }

        protected static bool isNullable(Type type)
        {
            if (!type.IsValueType) 
                return true;
            if (Nullable.GetUnderlyingType(type) != null) 
                return true;
            return false;
        }

        protected virtual void readColumn(SqlDataReader rdr, IObject obj, string columnName, PropertyInfo prop,object defaultValue)
        {
            try
            {
                int ordinal = rdr.GetOrdinal(columnName);

                if (rdr.IsDBNull(ordinal))
                {
                    if (defaultValue == null && !isNullable(prop.PropertyType))
                    {
                        throw new InvalidCastException(columnName + " is null but no default value is supplied for " + prop.Name);                        
                    }
                    else
                    {
                        try
                        {
                            prop.SetValue(obj, defaultValue);
                        }
                        catch (ArgumentException ex)
                        {
                            MethodInfo mi = prop.PropertyType.GetMethod("Parse",new Type[]{typeof(string)});
                            if (mi != null)
                            {
                                object val =
                                    mi.Invoke(null, new object[]{defaultValue.ToString()});
                                prop.SetValue(obj, val);
                            }
                            else
                            {
                                throw ex;
                            }
                        }
                    }
                }
                else
                {
                    prop.SetValue(obj,rdr.GetValue(ordinal));
                }


            }
            finally{

            }
            
            
        }

        

        protected virtual int readRow(SqlDataReader rdr, DataInterface dataInterface)
        {

            if (!rdr.HasRows || !rdr.Read())
            {
                sqlLog.warning("No rows returned");
                return 0;
            }


            foreach (PropertyInfo info in dataInterface.obj.GetType().GetProperties())
            {
                Sql.Column column = info.GetCustomAttribute<Sql.Column>();
                if (column != null)
                {
                    string name = column.name;
                    if (name == null)
                        name = info.Name;

                    DefaultValue defaultValueAttrib = info.GetCustomAttribute<DefaultValue>();
                    object defaultValue = null;
                    if (defaultValueAttrib != null)
                    {
                        defaultValue = defaultValueAttrib.value;
                    }

                    readColumn(rdr, dataInterface.obj, name, info, defaultValue);
                }
            }

            return 1;
        }



        public class ParameterBase:Attribute
        {
            public string name { get; protected set; }
            public System.Data.SqlDbType sqlType { get; protected set; }

            public ParameterBase(string name,System.Data.SqlDbType sqlType)
            {
                this.name = name;
                this.sqlType = sqlType;
            }

            public virtual object getValue(IObject obj,string name)
            {
                object buf;
                if (!obj.getElementValue<object>(out buf, name))
                {
                    string message = "Unable to get value for " + name + "on " + obj.GetType().AssemblyQualifiedName;
                    sqlLog.error(message);
                    throw new AttributeException(message);
                }
                return buf;
            }
        }
    }
}
