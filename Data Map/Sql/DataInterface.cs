﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Data.SqlTypes;
using System.Reflection;


namespace DataMap.Sql
{
    public class DataInterface : DataMap.IDataInterface 
    {
        protected static LogIt.ILog sqlLog = LogIt.LogIt.getLog("sql");


        public IObject obj { get; protected set; }
        public IDataMapFactory factory { get; protected set; }

        public DataInterface(IDataMapFactory factory, IObject obj)
        {
            this.factory = factory;
            this.obj = obj;

            buildColumnMap();
        }


        #region get/setElement

        public bool getElementValue<T>(out T value, [CallerMemberName] string name = null)
        {
            string columnName = name;

            Type objType = obj.GetType();
            PropertyInfo propertyInfo = objType.GetProperty(name);
            Column columnAttribute = propertyInfo.GetCustomAttribute<Column>(true);

            if (columnAttribute != null && columnAttribute.name != null)
            {
                columnName = columnAttribute.name;
            }

            if (!columnValues.ContainsKey(columnName))
            {
                DefaultValue defaultValue = propertyInfo.GetCustomAttribute<DefaultValue>(true);
                if (defaultValue != null)
                {
                    value = (T) defaultValue.value;
                    return true;
                }

                value = default(T);
                return false;

            }

            value = (T) columnValues[columnName];

            return true;
        }

        public bool setElementValue<T>(T value, [CallerMemberName] string name = null)
        {
            string columnName = name;

            Type objType = obj.GetType();
            PropertyInfo propertyInfo = objType.GetProperty(name);
            Column columnAttribute = propertyInfo.GetCustomAttribute<Column>(true);

            if (columnAttribute != null && columnAttribute.name != null)
            {
                columnName = columnAttribute.name;
            }

            columnValues[columnName] = value;
            return true;
        }

        #endregion


        #region validation

        public bool validate(out List<string> reasons, [CallerMemberName] string name = null)
        {

            IEnumerable<Validation.Validation> validations;
            object value;

            reasons = new List<string>();
            bool result = true;

            Type objType = obj.GetType();
            PropertyInfo propertyInfo = objType.GetProperty(name);

            validations = propertyInfo.GetCustomAttributes<Validation.Validation>(true);

            
            if (!getElementValue<object>(out value, name))
            {
                Validation.Required required = propertyInfo.GetCustomAttribute<Validation.Required>(true);

                if (required != null)
                {
                    reasons.Add(required.reason);
                    return false;
                }

            }

            foreach (Validation.Validation validation in validations)
            {
                string str;
                if (!validation.validate(value, out str))
                {
                    reasons.Add(str);
                    result = false;
                }
            }

            return result;

        }

        

        #endregion

        #region Columns

        protected Dictionary<string, object> columnValues { get; set; }

        protected class ColumnMapItem
        {
            public Column column { get; set; }
            
        }

        protected virtual void buildColumnMap()
        {
            this.columnValues = new Dictionary<string, object>();
        }

        #endregion


        #region SQL

        public SqlConnection connection
        {
            get
            {
                return ((SqlFactory)factory).connection;
            }
        }

        #endregion


        #region Commands

        public virtual bool commit()
        {
            Type type = obj.GetType();

            Command.ISqlCommand cmd=null;
            if (obj.isNew)
            {
                foreach (Attribute attrib in type.GetCustomAttributes())
                {
                    if (attrib is Command.IInsert)
                    {
                        if (cmd == null)
                        {
                            cmd = (Command.ISqlCommand)attrib;
                        }
                        else
                        {
                            string message = "Can not define multiple IInsert attributes for " + type.AssemblyQualifiedName;
                            sqlLog.error(message);
                            throw new AttributeException(message);
                        }
                    }
                }
                if (cmd == null)
                {
                    throw new AttributeException("No IInsert attribute specified on " + obj.GetType().Name);
                }
            }
            else
            {
                foreach (Attribute attrib in type.GetCustomAttributes())
                {
                    if (attrib is Command.IUpdate)
                    {
                        if (cmd == null)
                        {
                            cmd = (Command.ISqlCommand)attrib;
                        }
                        else
                        {
                            string message ="Can not define multiple IUpdate attributes for " + type.AssemblyQualifiedName;
                            sqlLog.error(message);
                            throw new AttributeException(message);
                        }
                    }
                }
                if (cmd == null)
                {
                    throw new AttributeException("No IUpdate attribute specified on " + obj.GetType().Name);
                }
            }

            try
            {
                bool result = cmd.execute(connection, this) == 1;

              
                return result;
            }
            catch (Exception ex)
            {
                sqlLog.error("Error commiting database object " + type.Name, ex);
                throw ex;
            }
        }

        public virtual bool delete()
        {
            Type type = obj.GetType();

            Command.ISqlCommand cmd = null;

            foreach (Attribute attrib in type.GetCustomAttributes())
            {
                if (attrib is Command.IDelete)
                {
                    if (cmd == null)
                    {
                        cmd = (Command.ISqlCommand)attrib;
                    }
                    else
                    {
                        string message = "Can not define multiple IDelete attributes for " + type.AssemblyQualifiedName;
                        sqlLog.error(message);
                        throw new AttributeException(message);
                        
                    }
                }
            }

            if (cmd == null)
            {
                throw new AttributeException("No IDelete attribute specified on " + obj.GetType().Name);
            }

            try
            {
                return cmd.execute(connection, this) >= 0;
            }
            catch (Exception ex)
            {
                sqlLog.error("Error deleting database object " + type.Name, ex);
                throw ex;
            }
        }

        public virtual bool refresh()
        {
            Type type = obj.GetType();

            Command.ISqlCommand cmd = null;

            foreach (Attribute attrib in type.GetCustomAttributes())
            {
                if (attrib is Command.ISelect)
                {
                    if (cmd == null)
                    {
                        cmd = (Command.ISqlCommand)attrib;
                    }
                    else
                    {
                        string message = "Can not define multiple IDelete attributes for " + type.AssemblyQualifiedName;
                        sqlLog.error(message);
                        throw new AttributeException(message);
                    }
                }
            }

            if (cmd == null)
            {
                throw new AttributeException("No ISelect attribute specified on " + obj.GetType().Name);
            }

            try
            {
                return cmd.execute(connection, this) == 1;
            }
            catch (Exception ex)
            {
                sqlLog.error("Error refreshing database object " + type.Name,ex);
                throw ex;
            }
        }

        public IEnumerable<T> getArray<T>([CallerMemberName] string propertyName = null) where T : IObject
        {

            Type objType = obj.GetType();
            PropertyInfo propType = objType.GetProperty(propertyName);
            Sql.Command.ISelectArray cmd = null;

            foreach (Attribute att in propType.GetCustomAttributes())
            {
                if (typeof(Sql.Command.ISelectArray).IsInstanceOfType(att))
                {
                    if (cmd == null)
                    {
                        cmd = (Sql.Command.ISelectArray)att;
                    }
                    else
                    {
                        string message = "Can not define multiple ISelectArray attributes for " + objType.AssemblyQualifiedName + "." + propType.Name;
                        sqlLog.error(message);
                        throw new AttributeException(message);
                    }
                }
            }

            if (cmd == null)
            {
                throw new AttributeException("No ISelectArray attribute specified on " + obj.GetType().Name+"."+propType.Name);
            }

            return cmd.executeArray<T>(connection, this, propertyName);

        }

        #endregion

       

        


        

    }
}
