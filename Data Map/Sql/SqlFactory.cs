﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Runtime.CompilerServices;
using System.Reflection;

namespace DataMap.Sql
{
    public class SqlFactory:DataMapFactoryBase
    {

        public SqlConnection connection { get; set; }

        protected override IDataInterface getInterfaceFor(DataMap.IObject obj)
        {
            return new DataInterface(this, obj);
        }

        public override IEnumerable<T> getArray<T>(Type type, [CallerMemberName] string methodName = null)
        {
            MethodInfo method = type.GetMethod(methodName);

            Command.ISelectArray cmd = null;

            foreach(Attribute att in method.GetCustomAttributes()){
                if (typeof(Command.ISelectArray).IsInstanceOfType(att))
                {
                    if (cmd == null)
                    {
                        cmd = att as Command.ISelectArray;
                    }
                    else
                    {
                        throw new AttributeException("Can not have multiple ISelectArray attributes on " + type.Name + "." + methodName);
                    }
                }
            }

            if (cmd == null)
            {
                throw new AttributeException("No ISelectArray attribute specified on "+type.Name+"."+methodName);
            }
            
            return cmd.executeArray<T>(this,type,methodName);
        }
        
    }
}
