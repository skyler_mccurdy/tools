﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace DataMap.Sql
{
    public class ReaderHelpers
    {
        protected static LogIt.ILog sqlLog = LogIt.LogIt.getLog("sql");

        public static string getString(SqlDataReader rdr, string column)
        {
            try
            {
                int ord = rdr.GetOrdinal(column);
                return rdr.GetString(ord);
            }
            catch (Exception e)
            {
                sqlLog.error("Can not conver column \"" + column + "\" to string ", e);
                return null;
            }
        }

        public static string getString(SqlDataReader rdr, string column, string defaultValue)
        {
            try
            {
                int ord = rdr.GetOrdinal(column);
                string tmp = rdr.GetString(ord);
                if (tmp == null)
                    return defaultValue;
                return tmp;
            }
            catch (Exception e)
            {
                sqlLog.error("Can not conver column \"" + column + "\" to string ", e);
                return defaultValue;
            }
        }

        public static int? getNullInt(SqlDataReader rdr, string column)
        {
            try
            {
                int ord = rdr.GetOrdinal(column);
                SqlInt32 tmp = rdr.GetInt32(ord);
                if (tmp.IsNull)
                    return null;
                return tmp.Value;
            }
            catch (Exception e)
            {
                sqlLog.error("Can not conver column \"" + column + "\" to int? ", e);
                return null;
            }

        }

        public static int getInt(SqlDataReader rdr, string column, int defaultValue)
        {
            int? tmp = getNullInt(rdr, column);
            if (tmp == null)
                return defaultValue;
            return tmp.Value;

        }

        public static int getInt(SqlDataReader rdr, string column)
        {
            int? tmp = getNullInt(rdr, column);
            if (tmp == null)
            {
                sqlLog.error("Can not convert column \"" + column + "\" to int");
                throw new InvalidCastException();
            }
            return tmp.Value;
        }

        public static double? getNullDouble(SqlDataReader rdr, string column)
        {
            try
            {
                int ord = rdr.GetOrdinal(column);
                SqlDouble tmp = rdr.GetDouble(ord);
                if (tmp.IsNull)
                    return null;
                return tmp.Value;
            }
            catch (Exception e)
            {
                sqlLog.error("Can not conver column \"" + column + "\" to double? ", e);
                return null;
            }

        }

        public static double getDouble(SqlDataReader rdr, string column, double defaultValue)
        {
            double? tmp = getNullDouble(rdr, column);
            if (tmp == null)
                return defaultValue;
            return tmp.Value;

        }

        public static double getDouble(SqlDataReader rdr, string column)
        {
            double? tmp = getNullDouble(rdr, column);
            if (tmp == null)
            {
                sqlLog.error("Can not convert column \"" + column + "\" to double");
                throw new InvalidCastException();
            }
            return tmp.Value;
        }

        public static decimal? getNullDecimal(SqlDataReader rdr, string column)
        {
            try
            {
                int ord = rdr.GetOrdinal(column);
                SqlDecimal tmp = rdr.GetDecimal(ord);
                if (tmp.IsNull)
                    return null;
                return tmp.Value;
            }
            catch (Exception e)
            {
                sqlLog.error("Can not conver column \"" + column + "\" to decimal? ", e);
                return null;
            }

        }

        public static decimal getDecimal(SqlDataReader rdr, string column, decimal defaultValue)
        {
            decimal? tmp = getNullDecimal(rdr, column);
            if (tmp == null)
                return defaultValue;
            return tmp.Value;

        }

        public static decimal getDecimal(SqlDataReader rdr, string column)
        {
            decimal? tmp = getNullDecimal(rdr, column);
            if (tmp == null)
            {
                sqlLog.error("Can not convert column \"" + column + "\" to decimal");
                throw new InvalidCastException();
            }
            return tmp.Value;
        }

        public static bool? getNullBool(SqlDataReader rdr, string column)
        {
            try
            {
                int ord = rdr.GetOrdinal(column);
                SqlBoolean tmp = rdr.GetBoolean(ord);
                if (tmp.IsNull)
                    return null;
                return tmp.Value;
            }
            catch (Exception e)
            {
                sqlLog.error("Can not conver column \"" + column + "\" to bool? ", e);
                return null;
            }

        }

        public static bool getBool(SqlDataReader rdr, string column, bool defaultValue)
        {
            bool? tmp = getNullBool(rdr, column);
            if (tmp == null)
                return defaultValue;
            return tmp.Value;

        }

        public static bool getBool(SqlDataReader rdr, string column)
        {
            bool? tmp = getNullBool(rdr, column);
            if (tmp == null)
            {
                sqlLog.error("Can not convert column \"" + column + "\" to bool");
                throw new InvalidCastException();
            }
            return tmp.Value;
        }

        public static DateTime? getNullDateTime(SqlDataReader rdr, string column)
        {
            try
            {
                int ord = rdr.GetOrdinal(column);
                return rdr.GetDateTime(ord);
            }
            catch (Exception e)
            {
                sqlLog.error("Can not conver column \"" + column + "\" to string ", e);
                return null;
            }
        }

        public static DateTime getDateTime(SqlDataReader rdr, string column, DateTime defaultValue)
        {
            DateTime? tmp = getNullDateTime(rdr, column);
            if (tmp == null)
                return defaultValue;
            return tmp.Value;

        }

        public static DateTime getDateTime(SqlDataReader rdr, string column)
        {
            DateTime? tmp = getNullDateTime(rdr, column);
            if (tmp == null)
            {
                sqlLog.error("Can not convert column \"" + column + "\" to DateTime");
                throw new InvalidCastException();
            }
            return tmp.Value;
        }

        public static object getSqlVariant(SqlDataReader rdr, string column)
        {
            try
            {
                int ord = rdr.GetOrdinal(column);

                object tmp = rdr.GetSqlValue(ord);
                if (tmp.GetType() == typeof(DBNull))
                    return null;
                return tmp;
            }
            catch (Exception e)
            {
                sqlLog.error("Can not conver column \"" + column + "\" to object ", e);
                return null;
            }
        }
    }
}
