﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataMap.Sql
{
    public class Column:Attribute
    {
        public string name { get; protected set; }


        public Column()
        {
            name = null;
        }

        public Column(string name)
        {
            this.name = name;
        }

    }
}
