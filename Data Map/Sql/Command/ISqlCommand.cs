﻿using System;
namespace DataMap.Sql.Command
{
    public interface ISqlCommand
    {
        int execute(global::System.Data.SqlClient.SqlConnection connection, global::DataMap.Sql.DataInterface obj);

    }
}
