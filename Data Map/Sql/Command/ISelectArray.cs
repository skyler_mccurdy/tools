﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataMap.Sql.Command
{
    public interface ISelectArray : ISqlCommand
    {

        IEnumerable<T> executeArray<T>(global::System.Data.SqlClient.SqlConnection connection, global::DataMap.Sql.DataInterface dataInterface,string propertyName) where T:IObject;
        IEnumerable<T> executeArray<T>(SqlFactory factory, Type type ,string propertyName) where T : IObject;



        
    }
}
