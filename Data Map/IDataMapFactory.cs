﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;

namespace DataMap
{
    public interface IDataMapFactory
    {
        T create<T>() where T : IObject;
        T create<T>(params object[] parameters) where T : IObject;

        T get<T>() where T : IObject;
        T get<T>(params object[] parameters) where T : IObject;

        IEnumerable<T> getArray<T>(Type type,[CallerMemberName] string propertyName = null) where T : IObject;
    }
}
