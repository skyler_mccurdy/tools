﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;


namespace DataMap
{
    public abstract class DataMapFactoryBase:IDataMapFactory
    {

        private class CacheObject
        {
            public WeakReference obj { get; set; }

            public CacheObject(object obj)
            {
                this.obj = new WeakReference(obj);
            }
        }

        private Dictionary<string, CacheObject> cache = new Dictionary<string, CacheObject>();


        public T create<T>() where T : IObject
        {
            return getObject<T>(false, null);
        }


        public T create<T>(object[] parameters) where T : IObject
        {
            return getObject<T>(false, parameters);
        }


        public T get<T>() where T : IObject
        {
            return getObject<T>(true, null);
        }

        public T get<T>(params object[] parameters) where T:IObject
        {
            T obj = getObject<T>(true,parameters);
            obj.refresh();
            return obj;
        }

        protected virtual T getObject<T>(bool useCache, object[] parameters) where T : IObject
        {
            string cacheId = null;
            if (useCache)
            {
                cacheId = typeof(T).AssemblyQualifiedName + "(";
                if (parameters != null)
                {
                    foreach (object p in parameters)
                    {
                        cacheId += ((p != null) ? p.ToString() : "null") + ",";
                    }
                }
                cacheId += ")";

                if (cache.ContainsKey(cacheId))
                {
                    if (cache[cacheId].obj.IsAlive)
                    {
                        return (T)cache[cacheId].obj.Target;
                    }

                }
            }

            IObject obj = (IObject) Activator.CreateInstance(typeof(T), parameters);
            obj.dataInterface = getInterfaceFor(obj);
            

            if (useCache)
            {
                cache[cacheId] = new CacheObject(obj);
            }

            return (T)obj;
        }

        public abstract IEnumerable<T> getArray<T>(Type type, [CallerMemberName] string propertyName = null) where T : IObject;

        protected abstract IDataInterface getInterfaceFor(IObject obj);

        private static Dictionary<Type, IDataMapFactory> factories = new Dictionary<Type, IDataMapFactory>();
        public static T factory<T>()
        {
            if (!factories.ContainsKey(typeof(T)))
            {
                
                factories[typeof(T)] = (IDataMapFactory)Activator.CreateInstance<T>();
                
            }
            return (T)factories[typeof(T)];
        }


        
    }
}
