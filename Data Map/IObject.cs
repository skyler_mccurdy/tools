﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace DataMap
{
    public interface IObject: INotifyPropertyChanged,INotifyDataErrorInfo
    {
        bool refresh();
        bool commit();
        bool delete();

        bool isNew { get; set; }
        bool isModified { get; set; }

        bool getElementValue<T>(out T value, [CallerMemberName] string name = null);
        bool setElementValue<T>(T value, [CallerMemberName] string name = null);
        

        bool validate(out List<string> reasons, [CallerMemberName] string name = null);
        bool validateAll(out List<string> reasons);

        IDataInterface dataInterface { get; set; }
        IDataMapFactory factory { get; }

    }
}
