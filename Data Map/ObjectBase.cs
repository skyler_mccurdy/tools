﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using System.ComponentModel;
using System.Collections;
using System.Reflection;

namespace DataMap
{
    public class ObjectBase:IObject
    {

        #region IObject
        public bool refresh()
        {
            bool result = dataInterface.refresh();

            if (result)
            {
                isNew = false;
                isModified = false;
            }

            return result;
        }

        public bool commit()
        {
            bool result = dataInterface.commit();
            if (result)
            {
                isNew = false;
                isModified = false;
            }

            return result;

        }

        public bool delete()
        {
            return dataInterface.delete();
        }

        public bool isNew { get; set; }

        public bool isModified { get; set; }


        public bool getElementValue<T>(out T value, [CallerMemberName] string name = null)
        {
            return dataInterface.getElementValue<T>(out value, name);
        }

        public bool setElementValue<T>(T value, [CallerMemberName] string name = null)
        {
            bool result = dataInterface.setElementValue<T>(value, name);

            if (result)
            {
                onPropertyChanged(name);
            }

            List<string> validationErrors;
            validate(out validationErrors,name);

            return result;
        }

        public bool validate(out List<string> reasons, [CallerMemberName] string name = null)
        {
            bool result = dataInterface.validate(out reasons, name);

            if (ErrorsChanged != null)
                ErrorsChanged(this, new DataErrorsChangedEventArgs(name));

            return result;
        }

        public bool validateAll(out List<string> reasons)
        {
            reasons = new List<string>();
            Type objType = this.GetType();
            bool result = true;

            foreach (PropertyInfo info in objType.GetProperties())
            {
                IEnumerable<Validation.Validation> validations;
                validations = info.GetCustomAttributes<Validation.Validation>(true);
                if (validations != null && validations.Count() > 0)
                {
                    List<string> buf;
                    if (!validate(out buf, info.Name))
                    {
                        foreach (string str in buf)
                        {
                            reasons.Add(info.Name + ": " + str);
                        }
                        return false;
                    }
                }
            }

            return result;

        }

        public IDataInterface dataInterface { get; set; }

        public IDataMapFactory factory
        {
            get
            {
                return dataInterface.factory;
            }
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected void onPropertyChanged([CallerMemberName] string propertyName = null)
        {

            PropertyChangedEventHandler eventHandler = this.PropertyChanged;
            if (eventHandler != null)
            {
                eventHandler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region INotifyDataErrorInfo
        // Summary:
        //     Gets a value that indicates whether the entity has validation errors.
        //
        // Returns:
        //     true if the entity currently has validation errors; otherwise, false.
        public bool HasErrors
        {
            get
            {
                List<string> buf;
                return validateAll(out buf);
            }
        }

        // Summary:
        //     Occurs when the validation errors have changed for a property or for the
        //     entire entity.
        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        // Summary:
        //     Gets the validation errors for a specified property or for the entire entity.
        //
        // Parameters:
        //   propertyName:
        //     The name of the property to retrieve validation errors for; or null or System.String.Empty,
        //     to retrieve entity-level errors.
        //
        // Returns:
        //     The validation errors for the property or entity.
        public IEnumerable GetErrors(string propertyName)
        {
            List<string> errors = new List<string>();

            if(propertyName != null && propertyName.Length > 0){
                List<string> buf;
                if (!validateAll(out buf))
                {
                    return buf;
                }
            }else{
                //TODO entity level errors
            }

            return errors;
        }

        #endregion
    }
}
