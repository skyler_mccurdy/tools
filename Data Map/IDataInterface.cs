﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections.Generic;

namespace DataMap
{
    public interface IDataInterface
    {
        bool refresh();
        bool commit();
        bool delete();
        IEnumerable<T> getArray<T>([CallerMemberName] string propertyName = null) where T : IObject;

        bool getElementValue<T>(out T value, [CallerMemberName] string name = null);
        bool setElementValue<T>(T value, [CallerMemberName] string name = null);

        bool validate(out List<string> reasons, [CallerMemberName] string name = null);

        IObject obj { get; }
        IDataMapFactory factory { get; }

    }
}
