﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataMap
{
    class AttributeException:Exception
    {
        public AttributeException(string message):
            base(message)
        {

        }
    }
}
