﻿using System;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test_Validation
{
    [TestClass]
    public class ValidateTest
    {



        [TestMethod]
        public void TestValidationAlwaysPasses()
        {
            int expected = 42;
            string reason;
            bool valid;
            PropertyInfo prop;

            Mocks.ValidationMockObject obj = new Mocks.ValidationMockObject();

            obj.testIntPass = expected;
            prop = obj.GetType().GetProperty("testIntPass");
            
            valid = Validation.Validation.runValidations(obj,prop,out reason);

            Assert.IsTrue(valid);
            Assert.AreEqual(string.Empty, reason);

        }

        [TestMethod]
        public void TestValidationAlwaysFails()
        {
            int value = 42;
            string reason;
            bool valid;
            PropertyInfo prop;

            Mocks.ValidationMockObject obj = new Mocks.ValidationMockObject();

            obj.testIntFails = value;
            prop = obj.GetType().GetProperty("testIntFails");

            valid = Validation.Validation.runValidations(obj, prop, out reason);

            Assert.IsFalse(valid);
            Assert.AreEqual("Failed", reason);

        }
    }
}
