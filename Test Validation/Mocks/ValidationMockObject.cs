﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_Validation.Mocks
{
    class ValidationMockObject
    {

        
        int _testIntPass=7;

        [MockValidationAlwaysPasses]
        public int testIntPass
        {
            get
            {
                return _testIntPass;
            }

            set
            {
                _testIntPass = value;
                
            }
        }

        
        int _testIntFails;
        [MockValidationAlwaysFails]
        public int testIntFails
        {
            get
            {
                return _testIntFails;
            }

            set
            {
                _testIntFails = value;
            }
        }



    }
}
