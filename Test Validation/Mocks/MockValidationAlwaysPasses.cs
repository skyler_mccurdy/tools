﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_Validation.Mocks
{
    class MockValidationAlwaysPasses: Validation.Validation
    {
        public override bool validate(object value, out string reason)
        {
            reason = "Ok";
            return true;
        }
    }
}
