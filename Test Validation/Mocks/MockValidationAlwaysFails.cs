﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_Validation.Mocks
{
    class MockValidationAlwaysFails:Validation.Validation
    {
        public override bool validate(object value, out string reason)
        {
            reason = "Failed";
            return false;
        }
    }
}
