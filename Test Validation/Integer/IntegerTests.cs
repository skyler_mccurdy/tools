﻿using System;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test_Validation.Integer
{
    [TestClass]
    public class IntegerTests
    {

        #region IsPositive
        [Validation.Integer.IsPositive]
        public int isPositive_Positive { get { return 42; } }

        [TestMethod]
        public void Validation_Integer_IsPositive()
        {

            string reason;
            bool valid;
            PropertyInfo prop;


            prop = GetType().GetProperty("isPositive_Positive");

            valid = Validation.Validation.runValidations(this, prop, out reason);

            Assert.IsTrue(valid);
            Assert.AreEqual(string.Empty, reason);

        }

        [Validation.Integer.IsPositive]
        public int isPositive_Negative { get { return -42; } }

        [TestMethod]
        public void Validation_Integer_IsPositive_WithNegative()
        {

            string reason;
            bool valid;
            PropertyInfo prop;


            prop = GetType().GetProperty("isPositive_Negative");

            valid = Validation.Validation.runValidations(this, prop, out reason);

            Assert.IsFalse(valid);
            Assert.AreEqual("must be greater than zero", reason);
        }
        #endregion

        #region IsNegative
        [Validation.Integer.IsNegative]
        public int isNegative_Positive { get { return 42; } }

        [TestMethod]
        public void Validation_Integer_IsNegative_WithPositive()
        {

            string reason;
            bool valid;
            PropertyInfo prop;


            prop = GetType().GetProperty("isNegative_Positive");

            valid = Validation.Validation.runValidations(this, prop, out reason);


            Assert.IsFalse(valid);
            Assert.AreEqual("must be less than zero", reason);
        }

        [Validation.Integer.IsNegative]
        public int isNegative_Negative { get { return -42; } }

        [TestMethod]
        public void Validation_Integer_IsNegative()
        {

            string reason;
            bool valid;
            PropertyInfo prop;


            prop = GetType().GetProperty("isNegative_Negative");

            valid = Validation.Validation.runValidations(this, prop, out reason);

            Assert.IsTrue(valid);
            Assert.AreEqual(string.Empty, reason);
        }
        #endregion

        #region NonZero
        [Validation.Integer.NonZero]
        public int nonZero { get { return 42; } }

        [TestMethod]
        public void Validation_Integer_NonZero()
        {

            string reason;
            bool valid;
            PropertyInfo prop;


            prop = GetType().GetProperty("nonZero");

            valid = Validation.Validation.runValidations(this, prop, out reason);

            Assert.IsTrue(valid);
            Assert.AreEqual(string.Empty, reason);
            
        }

        [Validation.Integer.NonZero]
        public int nonZero_Zero { get { return 0; } }

        [TestMethod]
        public void Validation_Integer_NonZero_WithZero()
        {

            string reason;
            bool valid;
            PropertyInfo prop;


            prop = GetType().GetProperty("nonZero_Zero");

            valid = Validation.Validation.runValidations(this, prop, out reason);

            Assert.IsFalse(valid);
            Assert.AreEqual("must not be zero", reason);
        }
        #endregion

        #region IsPositiveOrZero
        [Validation.Integer.IsPositiveOrZero]
        public int isPositiveOrZero_Positive { get { return 42; } }

        [TestMethod]
        public void Validation_Integer_IsPositiveOrZero()
        {

            string reason;
            bool valid;
            PropertyInfo prop;


            prop = GetType().GetProperty("isPositiveOrZero_Positive");

            valid = Validation.Validation.runValidations(this, prop, out reason);

            Assert.IsTrue(valid);
            Assert.AreEqual(string.Empty, reason);

        }

        [Validation.Integer.IsPositiveOrZero]
        public int isPositiveOrZero_zero { get { return 0; } }

        [TestMethod]
        public void Validation_Integer_IsPositiveOrZero_WithZero()
        {

            string reason;
            bool valid;
            PropertyInfo prop;


            prop = GetType().GetProperty("isPositiveOrZero_zero");

            valid = Validation.Validation.runValidations(this, prop, out reason);

            Assert.IsTrue(valid);
            Assert.AreEqual(string.Empty, reason);

        }

        [Validation.Integer.IsPositiveOrZero]
        public int isPositiveOrZero_Negative { get { return -42; } }

        [TestMethod]
        public void Validation_Integer_IsPositiveOrZero_WithNegative()
        {

            string reason;
            bool valid;
            PropertyInfo prop;


            prop = GetType().GetProperty("isPositiveOrZero_Negative");

            valid = Validation.Validation.runValidations(this, prop, out reason);

            Assert.IsFalse(valid);
            Assert.AreEqual("must be greater than or equal to zero", reason);
        }
        #endregion

        #region IsNegativeOrZero
        [Validation.Integer.IsNegativeOrZero]
        public int isNegativeOrZero { get { return -42; } }

        [TestMethod]
        public void Validation_Integer_IsNegativeOrZero()
        {

            string reason;
            bool valid;
            PropertyInfo prop;


            prop = GetType().GetProperty("isNegativeOrZero");

            valid = Validation.Validation.runValidations(this, prop, out reason);

            Assert.IsTrue(valid);
            Assert.AreEqual(string.Empty, reason);

        }

        [Validation.Integer.IsNegativeOrZero]
        public int isNegativeOrZero_zero { get { return 0; } }

        [TestMethod]
        public void Validation_Integer_IsNegativeOrZero_WithZero()
        {

            string reason;
            bool valid;
            PropertyInfo prop;


            prop = GetType().GetProperty("isNegativeOrZero_zero");

            valid = Validation.Validation.runValidations(this, prop, out reason);

            Assert.IsTrue(valid);
            Assert.AreEqual(string.Empty, reason);

        }

        [Validation.Integer.IsNegativeOrZero]
        public int isNegativeOrZero_Negative { get { return 42; } }

        [TestMethod]
        public void Validation_Integer_IsNegativeOrZero_WithPositive()
        {

            string reason;
            bool valid;
            PropertyInfo prop;


            prop = GetType().GetProperty("isNegativeOrZero_Negative");

            valid = Validation.Validation.runValidations(this, prop, out reason);

            Assert.IsFalse(valid);
            Assert.AreEqual("must be less than or equal to zero", reason);
        }
        #endregion

        #region GreaterThan
        [Validation.Integer.GreaterThan(8)]
        public int greaterThan { get { return 9; } }

        [TestMethod]
        public void Validation_Integer_GreaterThan()
        {

            string reason;
            bool valid;
            PropertyInfo prop;


            prop = GetType().GetProperty("greaterThan");

            valid = Validation.Validation.runValidations(this, prop, out reason);

            Assert.IsTrue(valid);
            Assert.AreEqual(string.Empty, reason);

        }

        [Validation.Integer.GreaterThan(8)]
        public int greaterThan_Less { get { return 7; } }

        [TestMethod]
        public void Validation_Integer_GreaterThan_Less()
        {

            string reason;
            bool valid;
            PropertyInfo prop;


            prop = GetType().GetProperty("greaterThan_Less");

            valid = Validation.Validation.runValidations(this, prop, out reason);

            Assert.IsFalse(valid);
            Assert.AreEqual("must be greater than 8", reason);

        }
        #endregion

        #region GreaterThanOrEqualTo
        [Validation.Integer.GreaterThanOrEqualTo(8)]
        public int greaterThanOrEqualTo { get { return 9; } }

        [TestMethod]
        public void Validation_Integer_GreaterThanOrEqualTo()
        {

            string reason;
            bool valid;
            PropertyInfo prop;


            prop = GetType().GetProperty("greaterThanOrEqualTo");

            valid = Validation.Validation.runValidations(this, prop, out reason);

            Assert.IsTrue(valid);
            Assert.AreEqual(string.Empty, reason);

        }

        [Validation.Integer.GreaterThanOrEqualTo(8)]
        public int greaterThanOrEqualTo_Less { get { return 7; } }

        [TestMethod]
        public void Validation_Integer_GreaterThanOrEqualTo_Less()
        {

            string reason;
            bool valid;
            PropertyInfo prop;


            prop = GetType().GetProperty("greaterThanOrEqualTo_Less");

            valid = Validation.Validation.runValidations(this, prop, out reason);

            Assert.IsFalse(valid);
            Assert.AreEqual("must be greater than or equal to 8", reason);

        }
        #endregion

        #region LessThan
        [Validation.Integer.LessThan(8)]
        public int lessThan { get { return 7; } }

        [TestMethod]
        public void Validation_Integer_LessThan()
        {

            string reason;
            bool valid;
            PropertyInfo prop;


            prop = GetType().GetProperty("lessThan");

            valid = Validation.Validation.runValidations(this, prop, out reason);

            Assert.IsTrue(valid);
            Assert.AreEqual(string.Empty, reason);

        }

        [Validation.Integer.LessThan(8)]
        public int lessThan_Greater { get { return 9; } }

        [TestMethod]
        public void Validation_Integer_LessThan_Greater()
        {

            string reason;
            bool valid;
            PropertyInfo prop;


            prop = GetType().GetProperty("lessThan_Greater");

            valid = Validation.Validation.runValidations(this, prop, out reason);

            Assert.IsFalse(valid);
            Assert.AreEqual("must be less than 8", reason);

        }
        #endregion

        #region LessThanOrEqualTo
        [Validation.Integer.LessThanOrEqualTo(8)]
        public int lessThanOrEqualTo { get { return 7; } }

        [TestMethod]
        public void Validation_Integer_LessThanOrEqualTo()
        {

            string reason;
            bool valid;
            PropertyInfo prop;


            prop = GetType().GetProperty("lessThanOrEqualTo");

            valid = Validation.Validation.runValidations(this, prop, out reason);

            Assert.IsTrue(valid);
            Assert.AreEqual(string.Empty, reason);

        }

        [Validation.Integer.LessThanOrEqualTo(8)]
        public int lessThanOrEqualTo_Greater { get { return 9; } }

        [TestMethod]
        public void Validation_Integer_LessThanOrEqualTo_Greater()
        {

            string reason;
            bool valid;
            PropertyInfo prop;


            prop = GetType().GetProperty("lessThanOrEqualTo_Greater");

            valid = Validation.Validation.runValidations(this, prop, out reason);

            Assert.IsFalse(valid);
            Assert.AreEqual("must be less than or equal to 8", reason);

        }
        #endregion
    }
}
