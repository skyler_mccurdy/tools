package com.oddzod.dataMap.tests.rest.client;

import static org.junit.Assert.*;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;

import org.junit.Test;

import com.oddzod.dataMap.DataMap;
import com.oddzod.dataMap.IDataFactory;
import com.oddzod.dataMap.baseType.ObjectBase;
import com.oddzod.dataMap.rest.Property;
import com.oddzod.dataMap.rest.actions.Delete;
import com.oddzod.dataMap.rest.actions.Get;
import com.oddzod.dataMap.rest.actions.GetArray;
import com.oddzod.dataMap.rest.actions.Post;
import com.oddzod.dataMap.rest.actions.Put;

public class TestDataMap {

	
	@Test
	public void DataMap_Rest_Client_GetArray() throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException, InvocationTargetException, IOException {
		
		
		IDataFactory factory = DataMap.getFactory(TestFactory.class);
		
		Mock[] mocks = Mock.getAll(factory);
		
		assertNotNull("mocks not null",mocks);
		assertEquals("count",3,mocks.length);
		for(int i=0;i<mocks.length;i++){
			assertEquals("id",i+1,mocks[i].getID());
			assertEquals("name","test "+(i+1),mocks[i].getName());
		}
	}
	
	@Test
	public void DataMap_Rest_Client_GetChildrenArray() throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException, InvocationTargetException, IOException {
		
		int id = 1;
		
		IDataFactory factory = DataMap.getFactory(TestFactory.class);
		
		
		Mock mock = (Mock)factory.get(Mock.class,id);
		
		Mock[] mocks = mock.getChildren();
		
		assertNotNull("mocks not null",mocks);
		assertEquals("count",3,mocks.length);
		for(int i=0;i<mocks.length;i++){
			assertEquals("id",(i+1)*100,mocks[i].getID());
			assertEquals("name","test "+(i+1)+"00",mocks[i].getName());
		}
	}
	
	@Test
	public void DataMap_Rest_Client_Get() throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException, InvocationTargetException, IOException {
		
		int id = 1;
		String name = "test 1";
		
		IDataFactory factory = DataMap.getFactory(TestFactory.class);
		
		Mock mock = (Mock)factory.get(Mock.class,id);
		
		assertNotNull("mock",mock);
		assertEquals("id",id,mock.getID());
		assertEquals("name",name,mock.getName());
	}
	
	@Test
	public void DataMap_Rest_Client_Put() throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException, InvocationTargetException, IOException {
		
		int id = 2;
		long time = (new Date()).getTime();
		String name = "test " + time;
		
		IDataFactory factory = DataMap.getFactory(TestFactory.class);
		
		Mock mock = (Mock)factory.get(Mock.class,id);
		mock.name = Long.toString(time);
		mock.commit();
		
		assertNotNull("mock",mock);
		assertEquals("id",id,mock.getID());
		assertEquals("name",name,mock.getName());
	}
	
	@Test
	public void DataMap_Rest_Client_Post() throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException, InvocationTargetException, IOException {
		
		int id = 3;
		String name = "test 3";
		
		IDataFactory factory = DataMap.getFactory(TestFactory.class);
		
		Mock mock = (Mock)factory.create(Mock.class);
		mock.name = "testing";
		mock.commit();
		
		assertNotNull("mock",mock);
		assertEquals("id",id,mock.getID());
		assertEquals("name",name,mock.getName());
	}
	
	@Test
	public void DataMap_Rest_Client_Delete() throws InstantiationException, IllegalAccessException, IllegalArgumentException, NoSuchMethodException, SecurityException, InvocationTargetException, IOException {
		
		int id = 4;
		
		IDataFactory factory = DataMap.getFactory(TestFactory.class);
		
		Mock mock = (Mock)factory.get(Mock.class,id);
		mock.delete();
		
		// it should pass here
	}
	
	@Get("test/{"+Mock.ID+"}")
	@Put("test/{"+Mock.ID+"}")
	@Post("test")
	@Delete("test/{"+Mock.ID+"}")
	public static class Mock extends ObjectBase{
		
		
		@GetArray("test")
		public static Mock[] getAll(IDataFactory factory) throws IllegalAccessException, IOException, InstantiationException{
			Method method;
			try {
				method = Mock.class.getMethod("getAll", new Class<?>[]{IDataFactory.class});
			} catch (Exception e) {
				throw new IllegalAccessException("unable to retrive method information for Mock.getAll");
			}
			return factory.getArray(Mock.class,method);
		}
		
		@GetArray("test?parent_ID={parent_id}")
		public Mock[] getChildren() throws IllegalAccessException, InstantiationException, IOException{
			Method method;
			try {
				method = Mock.class.getMethod("getChildren", (Class<?>[])null);
			} catch (Exception e) {
				throw new IllegalAccessException("unable to retrive method information for Mock.getChildren");
			}
			return getDataInterface().getArray(Mock.class,method);
		}
		

		public static final String ID = "id";
		public int id = -1;
		
		@Property.Get(name=ID)
		@Get.Parameter(ID)
		@Put.Parameter(ID)
		@Delete.Parameter(ID)
		@GetArray.Parameter(PARENT_ID)
		public int getID(){
			return id;
		}
		
		@Property.Set(name=ID)
		public void setID(int id){
			this.id = id;
		}
		
		public static final String PARENT_ID = "parent_id";
		public int parent_id = -1;
		
		@Property.Get(name=PARENT_ID)
		public int getParent_ID(){
			return parent_id;
		}
		
		@Property.Set(name=PARENT_ID)
		public void setParent_ID(int parent_id){
			this.parent_id = parent_id;
		}
		
		public static final String NAME = "name";
		public String name = null;
		
		@Property.Get(name=NAME)
		public String getName(){
			return name;
		}
		
		@Property.Set(name=NAME)
		public void setName(String name){
			this.name = name;
		}
		
		@Override
		public long getUniqueIdentifier() {
			return getID();
		}

		@Override
		protected void setUniqueIdentifier(long uniqueIdentifier) {
			setID((int)uniqueIdentifier);
		}
		
		
		public Mock(Long uniqueID) throws IOException{
			super(uniqueID);
			setID(uniqueID.intValue());
		}
		
		public Mock() throws IOException{
		}
		
	}

}
