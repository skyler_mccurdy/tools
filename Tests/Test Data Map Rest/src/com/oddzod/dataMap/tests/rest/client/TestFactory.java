package com.oddzod.dataMap.tests.rest.client;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import com.oddzod.dataMap.rest.IHttpConnection;
import com.oddzod.dataMap.rest.IHttpResult;
import com.oddzod.dataMap.rest.client.RestClientFactoryBase;

public class TestFactory extends RestClientFactoryBase{

	@Override
	public String getBaseURL() {
		return "https://localhost/";
	}
	
	@Override
	public IHttpConnection getConnection() {
		return new HttpConnection();
	}
	
	
	
	public class HttpConnection implements IHttpConnection{

		public STATE state = STATE.UNINITALIZED;
		public IHttpResult resultCallback = null;
		public String url = null;
		public String method = null;
		public Map<String,String> headers = new HashMap<String,String>();
		public Map<String,String> parameters = new HashMap<String,String>();
		public int timeout=10000;
		
		@Override
		public STATE getState() {
			return state;
		}

		@Override
		public IHttpResult getHttpResult() {
			return resultCallback;
		}

		@Override
		public void setHttpResult(IHttpResult resultObject) {
			this.resultCallback = resultObject;
		}

		@Override
		public String getURL() {
			return url;
		}

		@Override
		public void setURL(String url) {
			this.url = url;
		}

		@Override
		public String getMethod() {
			return method;
		}

		@Override
		public void setMethod(String method) {
			this.method = method;
		}

		@Override
		public String getHeader(String name) {
			if(headers.containsKey(name)){
				return headers.get(name);
			}
			return null;
		}

		@Override
		public void setHeader(String name, String value) {
			headers.put(name, value);
		}

		@Override
		public int getTimeout() {
			return timeout;
		}

		@Override
		public void setTimeout(int timeout) {
			this.timeout=timeout;
		}

		@Override
		public boolean connect() throws IOException {
			
			boolean success = false;
			String result = null;
			
			switch(getURL()){
			case "https://localhost/test":
				if(getMethod() == GET){
					success = true;
					result = "["+
								"{\"id\":1,\"name\":\"test 1\",\"parent_id\":1000},"+
								"{\"id\":2,\"name\":\"test 2\",\"parent_id\":1000},"+
								"{\"id\":3,\"name\":\"test 3\",\"parent_id\":1000}"+
							 "]";
				}
				if(getMethod() == POST){
					success = true;
					result = "{\"id\":3,\"name\":\"test 3\",\"parent_id\":1000}";
				}
				break;
				
			case "https://localhost/test?parent_ID=1":
				if(getMethod() == GET){
					success = true;
					result = "["+
								"{\"id\":100,\"name\":\"test 100\",\"parent_id\":1},"+
								"{\"id\":200,\"name\":\"test 200\",\"parent_id\":1},"+
								"{\"id\":300,\"name\":\"test 300\",\"parent_id\":1}"+
							 "]";
				}
				if(getMethod() == POST){
					success = true;
					result = "{\"id\":3,\"name\":\"test 3\",\"parent_id\":1000}";
				}
				break;
				
			case "https://localhost/test/1":
				if(getMethod() == GET){
					success = true;
					result = "{\"id\":1,\"name\":\"test 1\",\"parent_id\":1000}";
				}
				break;
				
			case "https://localhost/test/2":
				if(getMethod() == GET){
					success = true;
					result = "{\"id\":2,\"name\":\"test 2\",\"parent_id\":1000}";
				}
				if(getMethod() == PUT){
					success = true;
					result = "{\"id\":"+getParameter("id")+",\"name\":\"test "+getParameter("name")+"\",\"parent_id\":1000}";
				}
				break;
				
			case "https://localhost/test/3":
				if(getMethod() == GET){
					success = true;
					result = "{\"id\":3,\"name\":\"test 3\",\"parent_id\":1000}";
				}
				break;
			case "https://localhost/test/4":
				if(getMethod() == GET){
					success = true;
					result = "{\"id\":4,\"name\":\"test 4\",\"parent_id\":1000}";
				}
				if(getMethod() == DELETE){
					success = true;
					result = "{\"status\":\"ok\"}";
				}
				break;
			
			
			}
			
			if(success){
				InputStream stream = new ByteArrayInputStream(result.getBytes(StandardCharsets.UTF_8));
				
				return getHttpResult().onSuccess(this, 200, new HashMap<String,String>(), stream);
				
			}else{
				getHttpResult().onFailure(this, 404, null, null);
				return false;
			}
			
		}

		@Override
		public void close() {
			
		}

		@Override
		public String getParameter(String name) {
			if(parameters.containsKey(name)){
				return parameters.get(name);
			}
			return null;
		}

		@Override
		public void setParameter(String name, String value) {
			parameters.put(name,value);
		}
		
	}

	
}
