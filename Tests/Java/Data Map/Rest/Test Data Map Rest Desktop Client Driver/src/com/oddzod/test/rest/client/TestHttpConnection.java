package com.oddzod.test.rest.client;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.Map;

import org.junit.Test;

import com.oddzod.dataMap.rest.IHttpConnection;
import com.oddzod.dataMap.rest.IHttpResult;
import com.oddzod.dataMap.rest.client.RestClientFactoryBase;
import com.oddzod.dataMap.rest.client.httpDrive.RestClientFactory;

public class TestHttpConnection {

public static final String baseURL = "http://localhost/httpTest/";
	
	@Test
	public void testValidURL() throws IOException{
		
		RestClientFactoryBase factory = getFactory();
		
		IHttpConnection connection = factory.getConnection();
		
		connection.setURL("good.html");
		connection.setMethod(IHttpConnection.GET);
		boolean result  = connection.connect();
		
		assertTrue("connection",result);
		
		
	}
	
	@Test
	public void DataMap_Rest_Client_HttpConnection_Post() throws IOException{
		
		Callback cb = new Callback();
		String expected ="{\"theQuestion\":\"why?\",\"theAnswer\":\"42\"}";
		
		RestClientFactoryBase factory = getFactory();
		IHttpConnection connection = factory.getConnection();
		
		connection.setURL("post.php");
		connection.setMethod(IHttpConnection.POST);
		connection.setParameter("theQuestion", "why?");
		connection.setParameter("theAnswer", "42");
		connection.setHttpResult(cb);
		boolean result  = connection.connect();
		
		assertTrue("connection",result);
		assertEquals("result",expected,cb.result);
		
	}
	
	
	public RestClientFactoryBase getFactory() throws MalformedURLException{
		try{RestClientFactoryBase factory = new RestClientFactory();
		factory.setBaseURL( baseURL );
		
		return factory;
		}catch(Exception e){}
		return null;
	}
	
	public static class Callback implements IHttpResult{

		public String result = null;
		
		@Override
		public boolean onSuccess(IHttpConnection connection, int resultCode,
				Map<String, String> responseHeaders, InputStream inputStream)
				throws IOException {
			
			result = convertStreamToString(inputStream);
			
			return true;
		}

		@Override
		public void onFailure(IHttpConnection connection, int resultCode,
				Map<String, String> responseHeaders, InputStream inputStream) {
			
		}
		
		String convertStreamToString(java.io.InputStream is) {
		    @SuppressWarnings("resource")
			java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
		    return s.hasNext() ? s.next() : "";
		}
		
	}

}
