package com.oddzod.dataMap.rest.client;

import java.io.IOException;
import java.lang.reflect.Method;

import com.oddzod.dataMap.IDataInterface;
import com.oddzod.dataMap.IObject;
import com.oddzod.dataMap.baseType.DataFactoryBase;
import com.oddzod.dataMap.rest.IHttpConnection;
import com.oddzod.dataMap.rest.actions.GetArray;
import com.oddzod.dataMap.rest.actions.GetArray.GetArrayAction;
import com.oddzod.dataMap.rest.actions.restAction.RestClientAction;

public abstract class RestClientFactoryBase extends DataFactoryBase {

	public RestClientFactoryBase() {
		super();
	}

	@Override
	public IDataInterface getDataInterface(IObject obj) {
		return new DataInterface(obj);
	}
	
	/**
	 * Retrieves an array of specified type using the annotation provided on the supplied method
	 * 
	 * @param type the type of object to return
	 * @param method the method that called this
	 * @return
	 * @throws IOException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 */
	@Override
	public <T extends IObject> T[] getArray(Class<T> type,Method method) throws IllegalAccessException, IOException, InstantiationException{
		GetArray annotation = method.getAnnotation(GetArray.class);
		GetArrayAction action = (GetArrayAction) annotation.annotationType().getAnnotation(RestClientAction.class).value().newInstance();
		
		return action.executeArray(this,annotation,type);
	}
	
	public abstract IHttpConnection getConnection();

	protected String baseURL=null;
	
	public String getBaseURL(){
		return baseURL;
	}
	
	public void setBaseURL(String baseURL){
		this.baseURL=baseURL;
	}
}
