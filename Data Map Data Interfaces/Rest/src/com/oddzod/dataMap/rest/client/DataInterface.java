package com.oddzod.dataMap.rest.client;

import java.io.IOException;
import java.lang.reflect.Method;

import com.oddzod.dataMap.IObject;
import com.oddzod.dataMap.baseType.DataInterfaceBase;
import com.oddzod.dataMap.rest.actions.Delete;
import com.oddzod.dataMap.rest.actions.Get;
import com.oddzod.dataMap.rest.actions.GetArray;
import com.oddzod.dataMap.rest.actions.Post;
import com.oddzod.dataMap.rest.actions.Put;
import com.oddzod.dataMap.rest.actions.GetArray.GetArrayAction;
import com.oddzod.dataMap.rest.actions.restAction.RestClientAction;

public class DataInterface extends DataInterfaceBase {

	
	
	public DataInterface(IObject obj) {
		try {
			setObject(obj);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean refresh() throws IOException, IllegalAccessException {
		Get getAction = (Get) getObject().getClass().getAnnotation(Get.class);
		if(getAction==null){
			throw new IOException("No Get action defined for " + getObject().getClass().getName());
		}
		
		
		RestClientAction actionAnn = getAction.annotationType().getAnnotation(RestClientAction.class);
		Class<?> actionClass =  actionAnn.value();
		RestClientAction.IClientAction action = null;
		try {
			action = (RestClientAction.IClientAction) actionClass.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new IOException("Unable to initalize Get action for "+getObject().getClass().getName(),e);
		}
		
		action.execute(getObject(), getAction);
		
		return true;
	}

	@Override
	public boolean commit() throws IOException, IllegalAccessException {
		
		if(getObject().isNew()){
			Post postAction = (Post) getObject().getClass().getAnnotation(Post.class);
			if(postAction==null){
				throw new IOException("No Post action defined for " + getObject().getClass().getName());
			}
			
			
			RestClientAction actionAnn = postAction.annotationType().getAnnotation(RestClientAction.class);
			Class<?> actionClass =  actionAnn.value();
			RestClientAction.IClientAction action = null;
			try {
				action = (RestClientAction.IClientAction) actionClass.newInstance();
			} catch (InstantiationException | IllegalAccessException e) {
				throw new IOException("Unable to initalize put action for "+getObject().getClass().getName(),e);
			}
			
			action.execute(getObject(), postAction);
			return true;
		}else{
			Put putAction = (Put) getObject().getClass().getAnnotation(Put.class);
			if(putAction==null){
				throw new IOException("No Put action defined for " + getObject().getClass().getName());
			}
			
			
			RestClientAction actionAnn = putAction.annotationType().getAnnotation(RestClientAction.class);
			Class<?> actionClass =  actionAnn.value();
			RestClientAction.IClientAction action = null;
			try {
				action = (RestClientAction.IClientAction) actionClass.newInstance();
			} catch (InstantiationException | IllegalAccessException e) {
				throw new IOException("Unable to initalize put action for "+getObject().getClass().getName(),e);
			}
			
			action.execute(getObject(), putAction);
			return true;
		}
	}

	@Override
	public boolean delete() throws IOException, IllegalAccessException {
		Delete deleteAction = (Delete) getObject().getClass().getAnnotation(Delete.class);
		if(deleteAction==null){
			throw new IOException("No Delete action defined for " + getObject().getClass().getName());
		}
		
		
		RestClientAction actionAnn = deleteAction.annotationType().getAnnotation(RestClientAction.class);
		Class<?> actionClass =  actionAnn.value();
		RestClientAction.IClientAction action = null;
		try {
			action = (RestClientAction.IClientAction) actionClass.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new IOException("Unable to initalize delete action for "+getObject().getClass().getName(),e);
		}
		
		action.execute(getObject(), deleteAction);
		return true;
	}
	
	
	/**
	 * Retrieves an array of specified type using the annotation provided on the supplied method
	 * 
	 * @param type the type of object to return
	 * @param method the method that called this
	 * @return
	 * @throws IOException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 */
	@Override
	public <T extends IObject> T[] getArray(Class<T> type,Method method) throws IllegalAccessException, IOException, InstantiationException{
		GetArray annotation = method.getAnnotation(GetArray.class);
		GetArrayAction action = (GetArrayAction) annotation.annotationType().getAnnotation(RestClientAction.class).value().newInstance();
		
		return action.executeArray(getObject(),annotation,type);
	}
	

	@Override
	public void changed(String name) {
		
	}
	
	

}
