package com.oddzod.dataMap.rest.actions;

import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.oddzod.dataMap.IDataFactory;
import com.oddzod.dataMap.IObject;
import com.oddzod.dataMap.rest.IHttpConnection;
import com.oddzod.dataMap.rest.IHttpResult;
import com.oddzod.dataMap.rest.actions.restAction.RestClientAction;
import com.oddzod.dataMap.rest.client.RestClientFactoryBase;


@RestClientAction(GetArray.GetArrayAction.class)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface GetArray {
	
	public String value();
	
	
	@Retention(RetentionPolicy.RUNTIME)
	public @interface Parameter{
		public String value();
	}
	   
	public static class GetArrayAction extends RestClientAction.ClientAction{
		
		
		protected <T extends IObject> T[] executeArray(IDataFactory ifactory,Annotation annotation,Class<T> type,Map<String,Object> params) throws IllegalAccessException, IOException{
			RestClientFactoryBase factory = (RestClientFactoryBase) ifactory;
			
			GetArray cmd = (GetArray) annotation;
			URL url = buildURL(factory,cmd.value(),params);
						
			IHttpConnection con = factory.getConnection();
			con.setURL(url.toString());
			con.setMethod(IHttpConnection.GET);
			RestCallback<T> callback = new RestCallback<T>(factory,type);
			con.setHttpResult(callback);
			
			
			if(!con.connect()){
				throw new IOException("Unable to get array resource from "+url.toString());
			}
			
			return callback.getResult();
		}
		
		public <T extends IObject> T[] executeArray(IDataFactory ifactory,Annotation annotation,Class<T> type) throws IllegalAccessException, IOException{
			return executeArray(ifactory,annotation,type,new HashMap<String,Object>());
		}
		
		public <T extends IObject> T[] executeArray(IObject obj,Annotation annotation,Class<T> type) throws IllegalAccessException, IOException{
			Map<String,Object> params = getParameters(obj,GetArray.Parameter.class);
			return executeArray(obj.getFactory(),annotation,type,params);
		}
		
		
		protected class RestCallback<T extends IObject> implements IHttpResult{

			protected Class<T> type;
			protected IDataFactory factory;
			protected List<T> result = new ArrayList<T>();
			
			@SuppressWarnings("unchecked")
			public T[] getResult(){
				T[] out = (T[]) Array.newInstance(type,result.size());
				for(int i=0;i<result.size();i++){
					out[i]=result.get(i);
				}
				return out;
			}
			
			public RestCallback(IDataFactory factory,Class<T> type){
				this.factory=factory;
				this.type=type;
			}
			
			@Override
			public boolean onSuccess(IHttpConnection connection,
					int resultCode, Map<String, String> responseHeaders,
					InputStream inputStream) throws IOException {

				String json = convertStreamToString(inputStream);
				List<Map<String,Object>> jsonResult = parseJSONArray(json);
				
				try {
					
					for(int i=0;i<jsonResult.size();i++){
						@SuppressWarnings("unchecked")
						T obj = (T) factory.create(type);
						setPropertiesFrom(obj,jsonResult.get(i));
						result.add(obj);
					}
					
				} catch (IllegalAccessException e) {
					throw new IOException("IllegalAccessException",e);
				} catch (IllegalArgumentException e) {
					throw new IOException("IllegalArgumentException",e);
				} catch (InvocationTargetException e) {
					throw new IOException("InvocationTargetException",e);
				}catch (InstantiationException e) {
					throw new IOException("InstantiationException",e);
				}catch (NoSuchMethodException e) {
					throw new IOException("NoSuchMethodException",e);
				}
				
				return true;
			}

			@Override
			public void onFailure(IHttpConnection connection, int resultCode,
					Map<String, String> responseHeaders, InputStream inputStream) {

				
			}
			
			
			
		}

		@Override
		public void execute(IObject obj, Annotation cmd)
				throws IllegalAccessException, MalformedURLException,
				IOException {
			throw new IllegalAccessException("Not implemented");
		}
	}

}
