package com.oddzod.dataMap.rest.actions;

import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.net.URL;
import java.util.Map;

import com.oddzod.dataMap.IObject;
import com.oddzod.dataMap.rest.IHttpConnection;
import com.oddzod.dataMap.rest.IHttpResult;
import com.oddzod.dataMap.rest.actions.restAction.RestClientAction;
import com.oddzod.dataMap.rest.client.RestClientFactoryBase;


@RestClientAction(Delete.DeleteAction.class)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Delete {

	
	public String value();
	
	
	@Retention(RetentionPolicy.RUNTIME)
	public @interface Parameter{
		public String value();
	}
	
	public static class DeleteAction extends RestClientAction.ClientAction{
		
		public void execute(IObject obj,Annotation annotation) throws IllegalAccessException, IOException{
			
			Delete cmd = (Delete) annotation;
			Map<String,Object> params = getParameters(obj,Delete.Parameter.class);
			URL url = buildURL(obj.getFactory(),cmd.value(),params);
			
			RestClientFactoryBase factory = (RestClientFactoryBase)obj.getFactory();
			
			IHttpConnection con = factory.getConnection();
			con.setURL(url.toString());
			con.setMethod(IHttpConnection.DELETE);
			con.setHttpResult(new RestCallback(obj));
						
			if(!con.connect()){
				throw new IOException("Unable to delete resource from "+url.toString());
			}
		}
		
		protected class RestCallback implements IHttpResult{

			protected IObject obj;
			
			public RestCallback(IObject obj){
				this.obj=obj;
			}
			
			@Override
			public boolean onSuccess(IHttpConnection connection,
					int resultCode, Map<String, String> responseHeaders,
					InputStream inputStream) throws IOException {

				
				return true;
			}

			@Override
			public void onFailure(IHttpConnection connection, int resultCode,
					Map<String, String> responseHeaders, InputStream inputStream) {

				
			}
			
			
			
		}
	}
	
	
}
