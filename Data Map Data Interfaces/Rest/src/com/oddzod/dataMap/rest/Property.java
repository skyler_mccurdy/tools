package com.oddzod.dataMap.rest;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class Property {
	

	@Retention(RetentionPolicy.RUNTIME)
	public @interface Get{
		public String name();
	}

	
	@Retention(RetentionPolicy.RUNTIME)
	public @interface Set{
		public String name();
	}

}
