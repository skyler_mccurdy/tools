package com.oddzod.dataMap.rest;

import java.io.IOException;



public interface IHttpConnection {
	
	public STATE getState();
	
	public IHttpResult getHttpResult();
	public void setHttpResult(IHttpResult resultObject);
	
	public String getURL();
	public void setURL(String url);
	
	public String getMethod();
	public void setMethod(String method);
	
	public String getHeader(String name);
	public void setHeader(String name,String value);
	
	public String getParameter(String name);
	public void setParameter(String name,String value);
	
	public int getTimeout();
	public void setTimeout(int timeout);
	
	public boolean connect() throws IOException;
	
	public void close();
	
	public static final String GET = "GET";
	public static final String POST = "POST";
	public static final String PUT = "PUT";
	public static final String DELETE = "DELETE";
	
	public enum STATE{
		UNINITALIZED,
		CONNECTING,
		WAITING,
		SUCCESS,
		FAILED
	}
}
