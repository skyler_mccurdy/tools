package com.oddzod.dataMap.androidRestClient.test;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.Map;

import com.oddzod.dataMap.rest.*;
import com.oddzod.dataMap.rest.client.httpDrive.*;


import android.test.AndroidTestCase;

public class TestHttpConnection extends AndroidTestCase {

	public static final String baseURL = "http://10.10.10.10/httpTest/";
	
	public void testValidURL() throws IOException{
		
		String expected = "This is a good request";
		
		HttpResult httpResult = new HttpResult();
		
		com.oddzod.dataMap.rest.client.httpDrive.RestClientFactory factory = getFactory();
		
		IHttpConnection connection = factory.getConnection();
		
		connection.setURL("good.html");
		connection.setMethod(HttpConnection.GET);
		connection.setHttpResult(httpResult);
		
		boolean result  = connection.connect();
		
		assertTrue("connection",result);
		assertTrue("onSuccess",httpResult.onSuccessCalled);
		assertFalse("onFailure",httpResult.onFailureCalled);
		assertEquals(200,httpResult.resultCode);
		assertEquals(expected,httpResult.result);
		
	}
	
	public void testInvalidURL() throws IOException{
		
		
		HttpResult httpResult = new HttpResult();
		
		com.oddzod.dataMap.rest.client.httpDrive.RestClientFactory factory = getFactory();
		
		IHttpConnection connection = factory.getConnection();
		
		connection.setURL("bad.html");
		connection.setMethod(HttpConnection.GET);
		connection.setHttpResult(httpResult);
		
		try{
			connection.connect();
			
			fail("should have thrown exception");
			
		}catch(IOException e){
			//success
		}
		
		
		
	}
	
	public class HttpResult implements IHttpResult{
		
		public boolean onSuccessCalled = false;
		public boolean onFailureCalled = false;
		
		public String result=null;
		public int resultCode;

		@Override
		public boolean onSuccess(IHttpConnection connection, int resultCode,
				Map<String, String> responseHeaders, InputStream inputStream) {

			onSuccessCalled = true;
			
			this.resultCode=resultCode;
			result = convertStreamToString(inputStream);
			
			
			return true;
		}

		@Override
		public void onFailure(IHttpConnection connection, int resultCode,
				Map<String, String> responseHeaders, InputStream inputStream) {
			onFailureCalled = true;
		}
		
	}
	
	static String convertStreamToString(java.io.InputStream is) {
	    java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
	}
	
	
	public com.oddzod.dataMap.rest.client.httpDrive.RestClientFactory getFactory() throws MalformedURLException{
		try{
			while(getContext()==null)
				Thread.yield();
			com.oddzod.dataMap.rest.client.httpDrive.RestClientFactory factory = new com.oddzod.dataMap.rest.client.httpDrive.RestClientFactory();
			//factory.setContext(getContext());
			factory.setBaseURL(baseURL);
		
			return factory;
		}catch(Exception e){}
		return null;
	}
}
