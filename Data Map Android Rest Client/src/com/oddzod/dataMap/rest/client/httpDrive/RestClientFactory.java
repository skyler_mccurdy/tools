package com.oddzod.dataMap.rest.client.httpDrive;

import com.oddzod.dataMap.rest.IHttpConnection;
import com.oddzod.dataMap.rest.client.RestClientFactoryBase;

public class RestClientFactory extends RestClientFactoryBase {

	@Override
	public IHttpConnection getConnection() {
		return new HttpConnection(this);
	}

}
