﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogIt_Tests.Mocks
{
    class MockTarget:LogIt.Targets.TargetBase
    {

        public class LogEvent
        {
            public string topic { get; set; }
            public LogIt.LogLevel level { get; set; }
            public string message { get; set; }
            public Exception exception { get; set; }
        };

        public LogEvent last { get; set; }

        public MockTarget(): base()
        {
            
        }

        public MockTarget(LogIt.LogLevel level): base(level)
        {
            
        }

        public MockTarget(List<string> topics)
            : base(topics)
        {

        }

        public MockTarget(LogIt.LogLevel level, List<string> topics)
            : base(level,topics)
        {

        }

        protected override void logMessage(string topic, LogIt.LogLevel level, string message, Exception exception)
        {
            last = new LogEvent();
            last.topic = topic;
            last.level = level;
            last.message = message;
            last.exception = exception;

        }
    }
}
