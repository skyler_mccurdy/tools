﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LogIt;

namespace LogIt_Tests
{
    [TestClass]
    public class LogItTest
    {
        [TestMethod]
        public void LogError()
        {
            string topic = "Test Topic";
            string msg = "Test Message";
            Exception exception = null;

            Mocks.MockTarget target = new Mocks.MockTarget();
            ILog log = LogIt.LogIt.getLog(topic);
            LogIt.LogIt.targets.Clear();
            LogIt.LogIt.targets.Add(target);

            log.error(msg, exception);

            Assert.IsNotNull(target.last);
            Assert.AreEqual(LogLevel.Error, target.last.level);
            Assert.AreEqual(topic, target.last.topic);
            Assert.AreEqual(msg, target.last.message);
            Assert.AreEqual(exception, target.last.exception);
        }

        [TestMethod]
        public void LogErrorWithException()
        {
            string topic = "Test Topic";
            string msg = "Test Message";
            Exception exception = new Exception("Test Exception");

            Mocks.MockTarget target = new Mocks.MockTarget();
            ILog log = LogIt.LogIt.getLog(topic);
            LogIt.LogIt.targets.Clear();
            LogIt.LogIt.targets.Add(target);

            log.error(msg, exception);

            Assert.IsNotNull(target.last);
            Assert.AreEqual(LogLevel.Error, target.last.level);
            Assert.AreEqual(topic, target.last.topic);
            Assert.AreEqual(msg, target.last.message);
            Assert.AreEqual(exception, target.last.exception);
        }

       

        [TestMethod]
        public void LogWarningWithException()
        {
            string topic = "Test Topic";
            string msg = "Test Message";
            Exception exception = new Exception("Test Exception");

            Mocks.MockTarget target = new Mocks.MockTarget();
            ILog log = LogIt.LogIt.getLog(topic);
            LogIt.LogIt.targets.Clear();
            LogIt.LogIt.targets.Add(target);

            log.warning(msg, exception);

            Assert.IsNotNull(target.last);
            Assert.AreEqual(LogLevel.Warning, target.last.level);
            Assert.AreEqual(topic, target.last.topic);
            Assert.AreEqual(msg, target.last.message);
            Assert.AreEqual(exception, target.last.exception);
        }

        [TestMethod]
        public void LogInformationWithException()
        {
            string topic = "Test Topic";
            string msg = "Test Message";
            Exception exception = new Exception("Test Exception");

            Mocks.MockTarget target = new Mocks.MockTarget();
            ILog log = LogIt.LogIt.getLog(topic);
            LogIt.LogIt.targets.Clear();
            LogIt.LogIt.targets.Add(target);

            log.info(msg, exception);

            Assert.IsNotNull(target.last);
            Assert.AreEqual(LogLevel.Information, target.last.level);
            Assert.AreEqual(topic, target.last.topic);
            Assert.AreEqual(msg, target.last.message);
            Assert.AreEqual(exception, target.last.exception);
        }

        [TestMethod]
        public void LogDebugWithException()
        {
            string topic = "Test Topic";
            string msg = "Test Message";
            Exception exception = new Exception("Test Exception");

            Mocks.MockTarget target = new Mocks.MockTarget();
            ILog log = LogIt.LogIt.getLog(topic);
            LogIt.LogIt.targets.Clear();
            LogIt.LogIt.targets.Add(target);

            log.debug(msg, exception);

            Assert.IsNotNull(target.last);
            Assert.AreEqual(LogLevel.Debug, target.last.level);
            Assert.AreEqual(topic, target.last.topic);
            Assert.AreEqual(msg, target.last.message);
            Assert.AreEqual(exception, target.last.exception);
        }

        [TestMethod]
        public void LogWarning_LogLevelError()
        {
            string topic = "Test Topic";
            string msg = "Test Message";
            Exception exception = null;

            Mocks.MockTarget target = new Mocks.MockTarget(LogLevel.Error);
            ILog log = LogIt.LogIt.getLog(topic);
            LogIt.LogIt.targets.Clear();
            LogIt.LogIt.targets.Add(target);

            log.warning(msg, exception);

            Assert.IsNull(target.last);

        }

        [TestMethod]
        public void LogError_LogLevelWarning()
        {
            string topic = "Test Topic";
            string msg = "Test Message";
            Exception exception = null;

            Mocks.MockTarget target = new Mocks.MockTarget(LogLevel.Warning);
            ILog log = LogIt.LogIt.getLog(topic);
            LogIt.LogIt.targets.Clear();
            LogIt.LogIt.targets.Add(target);

            log.error(msg, exception);

            Assert.IsNotNull(target.last);
            Assert.AreEqual(LogLevel.Error, target.last.level);
            Assert.AreEqual(topic, target.last.topic);
            Assert.AreEqual(msg, target.last.message);
            Assert.AreEqual(exception, target.last.exception);
        }

        [TestMethod]
        public void LogError_InTopic()
        {
            string topic = "Test Topic";
            string msg = "Test Message";
            Exception exception = null;
            List<string> topics = new List<string>();
            topics.Add(topic);

            Mocks.MockTarget target = new Mocks.MockTarget(topics);
            ILog log = LogIt.LogIt.getLog(topic);
            LogIt.LogIt.targets.Clear();
            LogIt.LogIt.targets.Add(target);

            log.error(msg, exception);

            Assert.IsNotNull(target.last);
            Assert.AreEqual(LogLevel.Error, target.last.level);
            Assert.AreEqual(topic, target.last.topic);
            Assert.AreEqual(msg, target.last.message);
            Assert.AreEqual(exception, target.last.exception);
        }

        [TestMethod]
        public void LogError_NotInTopic()
        {
            string topic = "Test Topic";
            string msg = "Test Message";
            Exception exception = null;
            List<string> topics = new List<string>();
            topics.Add("foo");

            Mocks.MockTarget target = new Mocks.MockTarget(topics);
            ILog log = LogIt.LogIt.getLog(topic);
            LogIt.LogIt.targets.Clear();
            LogIt.LogIt.targets.Add(target);

            log.error(msg, exception);

            Assert.IsNull(target.last);
        }

        [TestMethod]
        public void LogError_MultipleTargets()
        {
            string topic = "Test Topic";
            string msg = "Test Message";
            Exception exception = null;
            List<string> topics1 = new List<string>();
            topics1.Add("foo");
            List<string> topics2 = new List<string>();
            topics2.Add(topic);

            Mocks.MockTarget target1 = new Mocks.MockTarget(topics1);
            Mocks.MockTarget target2 = new Mocks.MockTarget(topics2);
            Mocks.MockTarget target3 = new Mocks.MockTarget();
            ILog log = LogIt.LogIt.getLog(topic);
            LogIt.LogIt.targets.Clear();
            LogIt.LogIt.targets.Add(target1);
            LogIt.LogIt.targets.Add(target2);
            LogIt.LogIt.targets.Add(target3);

            log.error(msg, exception);

            Assert.IsNull(target1.last);

            Assert.IsNotNull(target2.last);
            Assert.AreEqual(LogLevel.Error, target2.last.level);
            Assert.AreEqual(topic, target2.last.topic);
            Assert.AreEqual(msg, target2.last.message);
            Assert.AreEqual(exception, target2.last.exception);

            Assert.IsNotNull(target3.last);
            Assert.AreEqual(LogLevel.Error, target3.last.level);
            Assert.AreEqual(topic, target3.last.topic);
            Assert.AreEqual(msg, target3.last.message);
            Assert.AreEqual(exception, target3.last.exception);
        }
    }
}
