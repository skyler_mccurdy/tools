﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LogIt.Targets;
using System.Data.SqlClient;


namespace LogIt_Tests
{
    [TestClass]
    public class DatabaseTargetTest
    {
        private static readonly string connectionString =
            //"Data Source=WIN81;Initial Catalog=ErrorLog;User ID=errorLogger;Password=DaG57rArchAteA";
            "Data Source=.;Initial Catalog=Test;Integrated Security=SSPI;";

        [TestMethod]
        public void logError()
        {
            DatabaseTarget target = new DatabaseTarget(connectionString);

            string topic = "Test Topic";
            string message = "Test Message";
            Exception exception = null;

            target.error(topic, message,exception);

            DBRow row = getLastRow();

            Assert.IsNotNull(row);
            Assert.AreEqual(message, row.message);
            Assert.AreEqual(topic, row.topic);

        }

        private DBRow getLastRow()
        {
            DBRow row = new DBRow();

            SqlConnection con = null;
            SqlCommand cmd = null;

            try
            {
                //con = new SqlConnection(connectionString);
                con = new SqlConnection();
                con.ConnectionString = connectionString;
                con.Open();
                cmd = new SqlCommand("select top 1 * from Log.Error order by timeOfError desc", con);
                

                SqlDataReader rdr = cmd.ExecuteReader();
                rdr.Read();

                row.error_ID = int.Parse(rdr["error_ID"].ToString());
                row.client = rdr["client"].ToString();
                row.username = rdr["username"].ToString();
                row.timeOfError = DateTime.Parse(rdr["timeOfError"].ToString()).ToUniversalTime();
                row.topic = rdr["topic"].ToString();
                row.message = rdr["errorMessage"].ToString();
                row.exception = rdr["exception"].ToString();
                row.stackTrace = rdr["stackTrace"].ToString();

                return row;
            }
            catch (Exception e)
            {
                e.ToString();
                return null;
            }
            finally
            {
                if (cmd==null)
                    cmd.Cancel();
                if (con==null)
                    con.Close();
            }
        }

        private class DBRow
        {
            public int error_ID { get; set; }
            public string client { get; set; }
            public string username { get; set; }
            public DateTime timeOfError { get; set; }
            public string topic { get; set; }
            public string message { get; set; }
            public string exception { get; set; }
            public string stackTrace { get; set; }
        }
    }
}
